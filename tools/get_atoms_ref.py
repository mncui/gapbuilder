import ase.io 
from ase import Atoms
import sys
import argparse
import numpy as np


def get_ref(name:str, atoms_for_formation_energy:Atoms, atoms_for_formation_energy_prefix:str, cell:list=None) -> Atoms:

    energy_tag0 = atoms_for_formation_energy_prefix + 'energy'
    energy_of_0 = atoms_for_formation_energy.info[energy_tag0]


    origin_atoms = ase.io.read(name, ':')
    for n, atoms in enumerate(origin_atoms):
        energy = atoms.info['energy']
        atoms.info['dft_energy'] = energy - energy_of_0
        atoms.set_array('dft_forces', atoms.get_array('forces'))
    return origin_atoms


def get_structure(name:str, atoms_for_formation_energy:Atoms, cell:list=None) -> Atoms:
    """
    Get the structure of the data
    - calculate relative energy
    - add xtb_energy, dft_forces tags
    - add cell to structure
    """
    energy_of_0 = atoms_for_formation_energy.info['xtb_energy']

    filename = f"{name}.npz"
    data = np.load(filename)
    E, F, R, z = data['E'], data['F'], data['R'], data['z']
    full_atoms = []
    for energy, force, coordinate, in zip(E, F, R):
        # print(energy, force, coordinate)
        atoms = Atoms(positions=coordinate, numbers=z)
        if cell != None:
            atoms.set_cell(cell)
            atoms.set_pbc(True)
        atoms.info['xtb_energy'] = energy - energy_of_0

        atoms.info['energy'] = energy
        atoms.set_array("xtb_forces", force)
        full_atoms.append(atoms)
    return full_atoms


parser = argparse.ArgumentParser(description='')

parser.add_argument('-ref', '--reference', nargs='+',
                    help="the reference structure file for relative energy.")

parser.add_argument('-mol', '--molecule', nargs='+',
                    help="the dataset that need to deal with.")
args = parser.parse_args()


atoms_for_relative_energy = ase.io.read(args.reference[0])
atoms_for_relative_energy_prefix = args.reference[1]
if atoms_for_relative_energy_prefix == '_':
    atoms_for_relative_energy_prefix = ''


cell = None
for dataset_name in args.molecule:
    name = dataset_name.split('.')[-2]
    origin_name = 'origin_' + dataset_name.split('.')[-2]


    # if npz file
    if dataset_name.split('.')[-1] == 'npz':
        full_atoms = get_structure(name, atoms_for_relative_energy, cell)
        ase.io.write(f"{origin_name}.xyz", full_atoms)
        print(f"output {origin_name}.xyz")

    # if xyz file
    elif dataset_name.split('.')[-1] == 'xyz':
        full_atoms = get_ref(dataset_name, atoms_for_relative_energy, atoms_for_relative_energy_prefix, cell)
        ase.io.write(f"{origin_name}.xyz", full_atoms)



    print(full_atoms[0].get_chemical_formula())
    print("Num:", len(full_atoms))
