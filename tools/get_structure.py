import sys
import numpy as np
from ase import Atoms
import ase.io

def get_structure(name:str, atoms_for_formation_energy:Atoms, cell:list=None) -> Atoms:
    """
    Get the structure of the data
    - calculate relative energy
    - add dft_energy, dft_forces tags
    - add cell to structure
    """
    energy_of_0 = atoms_for_formation_energy.info['dft_energy']

    filename = f"{name}.npz"
    data = np.load(filename)
    E, F, R, z = data['E'], data['F'], data['R'], data['z']
    full_atoms = []
    for energy, force, coordinate, in zip(E, F, R):
        # print(energy, force, coordinate)
        atoms = Atoms(positions=coordinate, numbers=z)
        if cell != None:
            atoms.set_cell(cell)
            atoms.set_pbc(True)
        atoms.info['dft_energy'] = energy - energy_of_0

        atoms.info['energy'] = energy
        atoms.set_array("dft_forces", force)
        full_atoms.append(atoms)
    return full_atoms

def random_idx(num:int, length:int, seed:int=None):
    """
    Generate random index that not duplicate from 0 to length-1 for splitting dataset

    Parameters:
    ----------
    num(int): the number of data in dataset
    
    seed(int): random seed
    """
    np.random.seed(seed)
    idx = np.random.choice(np.arange(length), num, replace=False)
    return idx

import argparse

parser = argparse.ArgumentParser(description='')

parser.add_argument('-ref', '--reference', type=str,
                    help="the reference structure file for relative energy.")

parser.add_argument('-mol', '--molecule', nargs='+',
                    help="the dataset that need to deal with.")

parser.add_argument('-cell', '--cell_size', type=int,
                    help="the size of cell for molecules")

args = parser.parse_args()


atoms_for_relative_energy = ase.io.read(args.reference)
#cell = [[17.25837925, 0, 0], [8.62899692, 14.9458635, 0], [0, 0, 38.70524185]] 
cell = None
for dataset_name in args.molecule:
    name = dataset_name.split('.')[-2]
    origin_name = 'origin_' + dataset_name.split('.')[-2]

    full_atoms = get_structure(name, atoms_for_relative_energy, cell)
    ase.io.write(f"{origin_name}.xyz", full_atoms)

    print(full_atoms[0].get_chemical_formula())
    print("Num:", len(full_atoms))
    print(f"output {origin_name}.xyz")

    # Pick up
    length = len(full_atoms)


    # print("Step 2: Pickup part of structures from dataset!")
    # if full_atoms[0].get_pbc().any():
    #     num = input("input exp: 100  ")
    #     num = int(num)
    if args.cell_size:
        # num, cell_size = input("input exp: 100 30  ").split(' ')
        num = length
        cell_size = float(args.cell_size)
        set_cell = [cell_size, cell_size, cell_size, 90, 90, 90]
    # filename, num, cell_size = sys.argv[1:]

    else:
        num = length
        set_cell = cell


    # Pick up
    idx = random_idx(int(num), length)

    # Add cell into 
    sub_dataset = []
    for i in idx:
        atoms = full_atoms[i]
        if not atoms.get_pbc().any():
            atoms.set_cell(set_cell)
        atoms.set_pbc(True)
        atoms.center()
        sub_dataset.append(atoms)

    ase.io.write(f"{name}.xyz", sub_dataset)
    # ase.io.write(full_atoms.replace('.xyz', f'_{num}.xyz'), sub_dataset)
    print(f"Randomly pickup {num} structures from {origin_name}.xyz and save to {name}.xyz")
