import ase.io
from ase import Atoms
import numpy as np
import sys


atoms_infiles = sys.argv[1:]

for atoms_infile in atoms_infiles:
    atoms_input = ase.io.read(atoms_infile, index=':')
    atoms_outfile = atoms_infile.replace('.xyz', '_shuff.xyz')
    
    
    # shuffle atoms_input
    indexs = np.arange(len(atoms_input))
    np.random.shuffle(indexs)


    dataset = []
    for n in indexs:
        dataset.append(atoms_input[n])
    ase.io.write(atoms_outfile, dataset)
    print(f"Saved {atoms_outfile}")

