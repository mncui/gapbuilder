import ase
import ase.io
import numpy as np
import matplotlib.pyplot as plt
import sys
import argparse


# use ASE to generate binary between atoms in each pair
# the bond length is set to 0.1A to 7.0A with 0.1A step
# https://matplotlib.org/stable/tutorials/introductory/customizing.html
plt.rcParams.update({"font.size": 15, 
                     "axes.linewidth": 2, # The width of the axes lines
                     "axes.labelsize": 15,
                     "axes.titlesize": 15,
                     "xtick.labelsize": 13,
                     "ytick.labelsize": 13,
                     "lines.linewidth": 2,
                     "lines.markersize": 5,
                     "legend.fontsize": 13,
                     "legend.markerscale": 2.0, # the relative size of legend markers vs. original
                     "legend.framealpha": 0.5,   # legend patch transparency
                    #  "scatter.markersize": 3
                     })
colors = [
    "#005555",  #0 fhi-aims green
    "#2470a0",  #1 muted blue
    "#ca3e47",  #2 muted red
    "#f29c2b",  #3 muted orange
    '#1f640a',  #4 muted green
    "#2ca02c",  #5 cooked asparagus green
    "#9467bd",  #6 muted purple
    "#8c564b",  #7 chestnut brown
    "#e377c2",  #8 raspberry yogurt pink
    "#7f7f7f",  #9 middle gray
    "#bcbd22",  #10 curry yellow-green
    "#17becf",  #11 blue-teal
]

markers = [
'o',#0       circle 
'v',#1       triangle_down marker
's',#2       square
'*',#3       star
'p',#4       point marker
'P',#5       pentagon marker
',',#6       pixel marker
'x',#7       x marker
]
linestyles = [
'-',#       solid line style
'--',#      dashed line style
'-.',#      dash-dot line style
':',#       dotted line stylecode 
(0, (3, 5, 1, 5, 1, 5)),#   dashdotdotted '- .. - .. - .. '
 (0, (5, 5)),#  dashed '-  -  -  -  -  -'
]





def get_binary_molecules(symbols:list, bond_length:list) -> list:
    """
    Generate binary molecules between atoms in each pair

    Parameters
    ----------
    symbols(list): list of atomic symbols
                   e.g. ['H', 'C']
    bond_length(list): list of bond length
                   e.g. np.arrange(0.1, 7.01, 0.1)
    
    Returns
    -------
    binary_molecules(list): list of binary molecules
    """
    binary_molecules = []
    for length in bond_length:
        atoms = ase.Atoms(symbols=''.join(symbols), positions=[(0, 0, 0), (0, 0, length)])
        atoms.set_cell([20, 20, 20])
        atoms.set_pbc([True, True, True])
        atoms.center()
        binary_molecules.append(atoms)
    return binary_molecules




from quippy.potential import Potential

def gap_calculator(atoms:list, param_filename='../fit/GAP.xml'):
    gap_soap = Potential(param_filename=param_filename)
    atoms_gap = []
    for i, mol in enumerate(atoms):
        mol.set_calculator(gap_soap)
        energy = mol.get_potential_energy()
        forces = mol.get_forces()
        mol.info['gap_energy'] = energy
        mol.set_array('gap_forces', forces)
        atoms_gap.append(mol)
        print(i, energy)
    return atoms_gap

def calc_energy_per_atom(atoms):
    """
    Calculate energy per atom

    """
    energy_per_atom = [mole.info['gap_energy']/len(mole) for mole in atoms]
    min_arg = np.argmin(energy_per_atom)
    return energy_per_atom, min_arg


# Plot subplots for each pair
def plot_binary(symbols:list, bond_length:list, atoms:list):
    n = len(symbols)
    fig, ax = plt.subplots(n,n, figsize=(10,10), dpi=150, sharex=True, sharey=False)
    for i in range(n):
        for j in range(n):
            if j > i:
                ax[i, j].axis('off')  # 隐藏不需要的坐标轴
                continue
            else:
                pair = f'{symbols[i]}-{symbols[j]}'
                argv = pair_symbols.index(pair)
                energy, arg = calc_energy_per_atom(atoms[argv*len(bond_length):(argv+1)*len(bond_length)])

                rel_dft_energy = np.array(energy) - energy[arg]

                ax[i,j].plot(bond_length, rel_dft_energy, label='DFT', color=colors[1], linestyle=linestyles[0])
                ax[i,j].set_title(pair)
            
    ax[n-1,n-1].legend()
    fig.tight_layout()
    # fig.show()
    fig.savefig('fbinary.png', dpi=300)


# get symbols and max bond length from command line
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--symbols', nargs='+', type=str, help='atomic symbols')
parser.add_argument('-max', '--max_bond_length', type=float, help='max bond length')
parser.add_argument('-calc', '--calculation', action='store_true', help='calculation')
parser.add_argument('-plot', '--plot', action='store_true', help='plot')
args = parser.parse_args()

symbols = args.symbols
max_bond_length = args.max_bond_length
# symbols = ['H', 'C', 'N', 'O']
pair_symbols = []
for i in range(len(symbols)):
    for j in range(0,i+1):
        pair_symbols.append(f'{symbols[i]}-{symbols[j]}')
print(pair_symbols)
bond_length = np.arange(0.3, max_bond_length, 0.2)

if args.calculation:
    total_molecules = []
    for pair in pair_symbols:
        symbol = pair.split('-')
        binary_molecules = get_binary_molecules(symbol, bond_length)
        total_molecules.extend(binary_molecules)
        
    ase.io.write('binary.xyz', total_molecules)

    # calculate energy and forces using GAP
    total_molecules_gap = gap_calculator(total_molecules)
    ase.io.write('binary_gap.xyz', total_molecules_gap)

if args.plot:
    # plot
    total_molecules_gap = ase.io.read('binary_gap.xyz', index=':')
    plot_binary(symbols, bond_length, total_molecules_gap)
    sys.exit()
