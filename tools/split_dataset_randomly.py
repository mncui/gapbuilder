import ase.io
from ase import Atoms
import numpy as np
import sys


atoms_infile, split_ratio = sys.argv[1:]
split_ratio = int(split_ratio)

atoms_input = ase.io.read(atoms_infile, index=':')


indexs = np.arange(len(atoms_input))
np.random.shuffle(indexs)
# print(indexs)

# split indexs_shuff into split_ratio parts
indexs_shuff_split = np.array_split(indexs, split_ratio)

for r in range(split_ratio):
    print(indexs_shuff_split[r])
    atoms_outfile = atoms_infile.replace('.xyz', f'_split_{r}.xyz')
    dataset = []
    for n in indexs_shuff_split[r]:
        dataset.append(atoms_input[n])
    ase.io.write(atoms_outfile, dataset)
    print(f"Saved {atoms_outfile}")
