import ase.io
import numpy as np
import matplotlib.pyplot as plt


# https://matplotlib.org/stable/tutorials/introductory/customizing.html
plt.rcParams.update({"font.size": 15, 
                     "axes.linewidth": 2, # The width of the axes lines
                     "axes.labelsize": 15,
                     "axes.titlesize": 15,
                     "xtick.labelsize": 13,
                     "ytick.labelsize": 13,
                     "lines.linewidth": 2,
                     "lines.markersize": 5,
                     "legend.fontsize": 13,
                     "legend.markerscale": 2.0, # the relative size of legend markers vs. original
                     "legend.framealpha": 0.5,   # legend patch transparency
                    #  "scatter.markersize": 3
                     })
colors = [
    "#005555",  #0 fhi-aims green
    "#2470a0",  #1 muted blue
    "#ca3e47",  #2 muted red
    "#f29c2b",  #3 muted orange
    '#1f640a',  #4 muted green
    "#2ca02c",  #5 cooked asparagus green
    "#9467bd",  #6 muted purple
    "#8c564b",  #7 chestnut brown
    "#e377c2",  #8 raspberry yogurt pink
    "#7f7f7f",  #9 middle gray
    "#bcbd22",  #10 curry yellow-green
    "#17becf",  #11 blue-teal
]

markers = [
'o',#0       circle 
'v',#1       triangle_down marker
's',#2       square
'*',#3       star
'p',#4       point marker
'P',#5       pentagon marker
',',#6       pixel marker
'x',#7       x marker
]
linestyles = [
'-',#        solid line style
'--',#       dashed line style
'-.',#       dash-dot line style
':',#       dotted line stylecode 
(0, (3, 5, 1, 5, 1, 5)),#   dashdotdotted '- .. - .. - .. '
 (0, (5, 5)),#  dashed '-  -  -  -  -  -'
]


def calculate_gyration_radius(atoms, exclude_elements=["H"]):
    """
    Calculate gyration radius w.r.t. center of mass for a given atoms object.

    Parameters:
        atoms (ase.Atoms): The atoms object.
        exclude_elements (list): List of element symbols to exclude.

    Returns:
        float: Gyration radius.
    """
    positions = atoms.positions
    masses = atoms.get_masses()
    symbols = atoms.get_chemical_symbols()

    # Exclude certain elements (e.g., Hydrogen)
    valid_indices = [i for i, symbol in enumerate(symbols) if symbol not in exclude_elements]

    # Compute center of mass
    total_mass = np.sum(masses[valid_indices])
    center_of_mass = np.sum(positions[valid_indices] * masses[valid_indices, np.newaxis], axis=0) / total_mass

    # Calculate gyration radius
    r_gyr_squared = np.sum(masses[valid_indices] * np.sum((positions[valid_indices] - center_of_mass)**2, axis=1)) / total_mass
    r_gyr = np.sqrt(r_gyr_squared)

    return r_gyr

import sys
# Read the MD trajectory
md_file = sys.argv[1]
md_trajectory = ase.io.read(md_file, index=':')

# Array to store gyration radius for each frame
gyration_radii = []

# Loop through trajectory and calculate gyration radius for each frame
for atoms in md_trajectory:
    r_gyr = calculate_gyration_radius(atoms)
    gyration_radii.append(r_gyr)

# Assuming you want to use NumPy to calculate statistics
mean_r_gyr = np.mean(gyration_radii)
std_r_gyr = np.std(gyration_radii)

print(f"Mean Gyration Radius: {mean_r_gyr}")
print(f"Standard Deviation: {std_r_gyr}")


plt.hist(gyration_radii, bins=100, density=True)
# annotate the mean and standard deviation
plt.axvline(mean_r_gyr, color='k', linestyle='dashed', linewidth=1)
plt.axvline(mean_r_gyr + std_r_gyr, color='k', linestyle='dashed', linewidth=1)
plt.axvline(mean_r_gyr - std_r_gyr, color='k', linestyle='dashed', linewidth=1)

# annotate the mean and standard deviation on the plot
plt.text(0.1, 0.5, f"Mean: {mean_r_gyr:.2f}", transform=plt.gca().transAxes)
plt.text(0.1, 0.45, f"Standard Deviation: {std_r_gyr:.2f}", transform=plt.gca().transAxes)



plt.xlabel("Radius of Gyration (Å)")
plt.ylabel("Probability Density")
plt.title("Distribution of Gyration Radius (Hydrogens Excluded)")
plt.tight_layout()
plt.savefig("grd_hist.png", dpi=300)

# Plot gyration radius vs. time
plt.clf()
time_step = 0.5
time = np.arange(0, len(gyration_radii)) * time_step

plt.plot(time, gyration_radii)
plt.xlabel("Time (fs)")
plt.ylabel("Radius of Gyration (Å)")
plt.title("Radius of Gyration vs. Time")
plt.tight_layout()
plt.savefig("grd_time.png", dpi=300)
