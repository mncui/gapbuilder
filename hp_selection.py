import os
import json
import ase.io
import numpy as np
import argparse
import sys
import matplotlib.pyplot as plt

# https://matplotlib.org/stable/tutorials/introductory/customizing.html
plt.rcParams.update({"font.size": 15, 
                     "axes.linewidth": 2, # The width of the axes lines
                     "axes.labelsize": 15,
                     "axes.titlesize": 15,
                     "xtick.labelsize": 13,
                     "ytick.labelsize": 13,
                     "lines.linewidth": 2,
                     "lines.markersize": 5,
                     "legend.fontsize": 13,
                     "legend.markerscale": 2.0, # the relative size of legend markers vs. original
                     "legend.framealpha": 0.5,   # legend patch transparency
                    #  "scatter.markersize": 3
                     })
colors = [
    "#005555",  #0 fhi-aims green
    "#2470a0",  #1 muted blue
    "#ca3e47",  #2 muted red
    "#f29c2b",  #3 muted orange
    '#1f640a',  #4 muted green
    "#2ca02c",  #5 cooked asparagus green
    "#9467bd",  #6 muted purple
    "#8c564b",  #7 chestnut brown
    "#e377c2",  #8 raspberry yogurt pink
    "#7f7f7f",  #9 middle gray
    "#bcbd22",  #10 curry yellow-green
    "#17becf",  #11 blue-teal
    "black",
]

markers = [
'o',#0       circle 
'v',#1       triangle_down marker
's',#2       square
'*',#3       star
'p',#4       point marker
'P',#5       pentagon marker
',',#6       pixel marker
'x',#7       x marker
]
linestyles = [
'-',#        solid line style
'--',#       dashed line style
'-.',#       dash-dot line style
':',#       dotted line stylecode 
(0, (3, 5, 1, 5, 1, 5)),#   dashdotdotted '- .. - .. - .. '
 (0, (5, 5)),#  dashed '-  -  -  -  -  -'
(0, (1, 10)),#  loosely dotted '.   .   .   .'
]

def jobs_submit(time:str="23:59:00", 
                 disk_space:int=100000,
                 job_name:str="gap-fit",
                 partition:str="middle",
                 core:int=40,
                 mpcdf:str='raven',
                 run_command:str="python run.py -gap -val -err > py.out"):
    """
    Write submition script for cobra

    Parameters:
    ----------
    time(str): time for job
        e.g. "23:59:00"

    disk_space(int): disk space for job
        e.g. 100000
    
    job_name(str): job name
        e.g. "gap-fit"

    Returns:
    -------
    submit.sh: submition script for raven
    
    """
    modules = {'raven':"""
eval "$(conda shell.bash hook)"          
conda activate gap_workflow              
export PYTHONPATH=/u/mncui/software/anaconda3/envs/gap_workflow/bin/python:{PYTHONPATH}

module purge                                                      
module load gcc/11 mkl/2022.2 gsl/2.4 impi/2021.4 fftw-mpi/3.3.9 
module load parallel/201807
source ${MKLROOT}/env/vars.sh intel64                           
export QUIP_ARCH=linux_x86_64_gfortran_openmp                     
export QUIP_ROOT=/u/mncui/software/QUIP                           
export QUIPPY_INSTALL_OPTS=--user

export LD_PRELOAD=/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_core.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_intel_thread.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_intel_lp64.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/compiler/2022.2.1/linux/compiler/lib/intel64_lin/libiomp5.so
               
## source {MKLROOT}/bin/mklvars.sh intel64
export OMP_NUM_THREADS=1
export WFL_GAP_FIT_OMP_NUM_THREADS=72
""", 
    'cobra':"""
eval "$(conda shell.bash hook)"          
conda activate gap_workflow              

module purge                                                      
module load gcc/9 mkl/2020.1 gsl/2.4 impi/2019.7 fftw-mpi/3.3.9
module load parallel/201807
#source ${MKLROOT}/env/vars.sh intel64                           
source ${MKLROOT}/bin/mklvars.sh intel64
export QUIP_ARCH=linux_x86_64_gfortran_openmp                     
export QUIP_ROOT=/u/mncui/software/QUIP                           
export QUIPPY_INSTALL_OPTS=--user

export LD_PRELOAD=/mpcdf/soft/SLE_12/packages/x86_64/intel_parallel_studio/2020.1/compilers_and_libraries_2020.1.217/linux/mkl/lib/intel64_lin/libmkl_core.so:/mpcdf/soft/SLE_12/packages/x86_64/intel_parallel_studio/2020.1/compilers_and_libraries_2020.1.217/linux/mkl/lib/intel64_lin/libmkl_intel_thread.so:/mpcdf/soft/SLE_12/packages/x86_64/intel_parallel_studio/2020.1/compilers_and_libraries_2020.1.217/linux/mkl/lib/intel64_lin/libmkl_intel_lp64.so:/mpcdf/soft/SLE_12/packages/x86_64/intel_parallel_studio/2020.1/compilers_and_libraries_2020.1.217/linux/compiler/lib/intel64_lin/libiomp5.so
## source {MKLROOT}/bin/mklvars.sh intel64
export OMP_NUM_THREADS=1
export WFL_GAP_FIT_OMP_NUM_THREADS=40
"""}

    script_content = f"""#!/bin/bash -l
#SBATCH --no-requeue                    
#SBATCH --nodes=1                         
#SBATCH --ntasks-per-node={core}                      
#SBATCH --job-name={job_name}            
#SBATCH --partition=general              
#SBATCH --time={time}                    
#SBATCH -o ./tjob.out.%j                 
#SBATCH -e ./tjob.err.%j                 
#SBATCH --mem={disk_space}               
                                         
{modules[mpcdf]}

# Run Forest, run...
which python
{run_command}
"""

    # Writing to a file
    with open(f"submit.sh", "w") as f:
        f.write(script_content)

    return 0

def vary_2b_cutoff(ref_val_files:list, fitting_dict:dict, cutoffs:list):


    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]


    for cutoff in cutoffs:
        folder_name = f"cutoff_{cutoff}"
        # fitting_dict['_gap'][0]["cutoff"] = cutoff
        fitting_dict['stages'][0]["descriptors"][0]["descriptor"]["cutoff"] = f"_EVAL_ {{BOND_LEN_Z1_Z2}}*{cutoff}"

        if os.path.exists(folder_name):
            os.chdir(folder_name)
        else:
            os.mkdir(folder_name)
            os.chdir(folder_name)
        
        # Copy ref_dataset_file 
        os.system(f"cp ../input/{ref_dataset_file} .")
        os.system(f"cp ../input/{val_dataset_file} .")
        os.system(f"cp ../input/run.py .")

        # write new fitting_dict to file
        with open("multistage_gap_parameters.json", "w") as f:
            json.dump(fitting_dict, f, indent=4)

        # Run 2b locally
        run_command = "python run.py -gap -val -err > py.out"
        # os.system(f"sbatch submit.sh")
        os.system(f"{run_command}")

        os.chdir("..")
        
        # error = fit_and_error(ref_dataset_file, val_dataset_file, ref_property, gap_property, fitting_dict)
    return 0


def vary_2b_nsparse(ref_val_files:list, fitting_dict:dict, sparses:list):

    """
    Hyperparameter selection for 2-body: delta and sparse

    Parameters:
    ----------
    fitting_dict(dict): dictionary for GAP fitting parameters

    deltas(list): list of delta for 2-body
        e.g. [1.0, 2.0, 3.0]

    sparses(list): list of sparse for 2-body
        
    """

    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]

    for sparse in sparses:
        folder_name = f"nsparse_{sparse}"
        fitting_dict['stages'][0]["descriptors"][0]["fit"]["n_sparse"] = sparse


        if os.path.exists(folder_name):
            os.chdir(folder_name)
        else:
            os.mkdir(folder_name)
            os.chdir(folder_name)
        
        # Copy ref_dataset_file
        os.system(f"cp ../input/{ref_dataset_file} .")
        os.system(f"cp ../input/{val_dataset_file} .")
        os.system(f"cp ../input/run.py .")

        # write new fitting_dict to file
        with open("multistage_gap_parameters.json", "w") as f:
            json.dump(fitting_dict, f, indent=4)

        
        run_command = "python run.py -gap -val -err > py.out"
        # os.system(f"sbatch submit.sh")
        os.system(f"{run_command}") 
        os.chdir("..")

    return 0


def vary_2b_delta_and_sparse(ref_val_files:list, fitting_dict:dict, deltas:list, sparses:list):

    """
    Hyperparameter selection for 2-body: delta and sparse

    Parameters:
    ----------
    fitting_dict(dict): dictionary for GAP fitting parameters

    deltas(list): list of delta for 2-body
        e.g. [1.0, 2.0, 3.0]

    sparses(list): list of sparse for 2-body
        
    """

    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]

    for delta in deltas:
        for sparse in sparses:
            folder_name = f"delta_sparse_{delta}_{sparse}"
            fitting_dict['_gap'][0]["delta"] = delta
            fitting_dict['_gap'][0]["n_sparse"] = sparse


            if os.path.exists(folder_name):
                os.chdir(folder_name)
            else:
                os.mkdir(folder_name)
                os.chdir(folder_name)
            
            # Copy ref_dataset_file
            os.system(f"cp ../input/{ref_dataset_file} .")
            os.system(f"cp ../input/{val_dataset_file} .")
            os.system(f"cp ../input/run.py .")

            # write new fitting_dict to file
            with open("gap_fit_parameters.json", "w") as f:
                json.dump(fitting_dict, f, indent=4)

            
            run_command = "python run.py -gap train.xyz -val validation.xyz -err validation.xyz train.xyz > py.out"
            # Write submit.sh ans submit job
            jobs_submit(time=submit_dict['time'],
                 disk_space=submit_dict['disk_space'],
                 job_name=submit_dict['job_name'],
                 partition=submit_dict['partition'],
                 core=submit_dict['core'],
                 run_command=run_command)
            os.system(f"sbatch submit.sh")
            os.chdir("..")

    return 0


def vary_nl_max(ref_val_files:list, fitting_dict:dict, n_maxs:list, l_maxs:list):
    """
    Hyperparameter selection for SOAP: n_max and l_max

    Parameters:
    ----------
    ref_val_files(list): list of reference and validation dataset files
        e.g. ["train.xyz", "validation.xyz"]

    fitting_dict(dict): dictionary for GAP fitting parameters

    n_maxs(list): list of n_max for SOAP
        e.g. [7, 8, 9, 10]

    l_maxs(list): list of l_max for SOAP


    Returns:       
    -------
    """

    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]

    for n_max in n_maxs:
        for l_max in l_maxs:
            folder_name = f"nlmax_{n_max}_{l_max}"
            fitting_dict['stages'][0]["descriptors"][0]["descriptor"]["n_max"] = str(n_max)
            fitting_dict['stages'][0]["descriptors"][0]["descriptor"]["l_max"] = str(l_max)

            if os.path.exists(folder_name):
                os.chdir(folder_name)
            else:
                os.mkdir(folder_name)
                os.chdir(folder_name)
            
            # Copy ref_dataset_file
            os.system(f"cp ../input/{ref_dataset_file} train.xyz")
            os.system(f"cp ../input/{val_dataset_file} validation.xyz")
            os.system(f"cp ../input/run.py .")

            # write new fitting_dict to file
            with open("multistage_gap_parameters.json", "w") as f:
                json.dump(fitting_dict, f, indent=4)

            run_command = "python run.py -gap train.xyz -val train.xyz validation.xyz -err validation_gap.xyz train_gap.xyz > py.out"
            # Write submit.sh ans submit job
            jobs_submit(time=submit_dict['time'],
                 disk_space=submit_dict['disk_space'],
                 job_name=submit_dict['job_name'],
                 partition=submit_dict['partition'],
                 core=submit_dict['core'],
                 mpcdf=submit_dict['mpcdf'],
                 run_command=run_command)
            os.system(f"sbatch submit.sh")

            os.chdir("..")

    return 0


def vary_nsparse(ref_val_files:list, fitting_dict:dict, sparses:list, btag:str):
    """
    Hyperparameter selection for SOAP: n_sparse

    Parameters:
    ----------
    ref_val_files(list): list of reference and validation dataset files
        e.g. ["train.xyz", "validation.xyz"]
    
    fitting_dict(dict): dictionary for GAP fitting parameters

    sparses(list): list of sparse for SOAP
        e.g. [50, 100, 500, 1000, 2000, 2500, 3000, 4000, 5000]
    
    btag(str): tag for 2-body or SOAP
        e.g. "2b" or "soap"
    """

    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]

    for sparse in sparses:
        folder_name = f"nsparse_{sparse}"

        if btag == "2b":
            fitting_dict['stages'][0]["descriptors"][0]["fit"]["n_sparse"] = sparse
        elif btag == "soap":
            fitting_dict['stages'][1]["descriptors"][0]["fit"]["n_sparse"] = sparse
        else:
            raise ValueError("btag must be 2b or soap")

        if os.path.exists(folder_name):
            os.chdir(folder_name)
        else:
            os.mkdir(folder_name)
            os.chdir(folder_name)
        
        # Copy ref_dataset_file
        os.system(f"cp ../input/{ref_dataset_file} .")
        os.system(f"cp ../input/{val_dataset_file} .")
        os.system(f"cp ../input/run.py .")

        # write new fitting_dict to file
        with open("multistage_gap_parameters.json", "w") as f:
            json.dump(fitting_dict, f, indent=4)

        
        run_command = "python run.py -gap train.xyz -val train.xyz validation.xyz -err validation_gap.xyz train_gap.xyz > py.out"
        # Write submit.sh ans submit job

        if btag == "2b":
            os.system(f"{run_command}")
        elif btag == "soap":
            jobs_submit(time=submit_dict['time'],
                 disk_space=submit_dict['disk_space'],
                 job_name=submit_dict['job_name'],
                 partition=submit_dict['partition'],
                 core=submit_dict['core'],
                 mpcdf=submit_dict['mpcdf'],
                 run_command=run_command)
            
            os.system(f"sbatch submit.sh")
        # os.system(f"{run_command}") 
        os.chdir("..")

def vary_sigma(ref_val_files:list, fitting_dict:dict, sigma_E:list, ratio_E_F:list):
    """
    Hyperparameter selection for SOAP: sigma_E, sigma_F

    Parameters:
    ----------
    ref_val_files(list): list of reference and validation dataset files
        e.g. ["train.xyz", "validation.xyz"]

    fitting_dict(dict): dictionary for GAP fitting parameters

    sigma_E(list): list of sigma_E for SOAP
        e.g. [10e-5, 10e-4, 10e-3, 10e-2, 10e-1]
    
    ratio_E_F(list): list of ratio_E_F for SOAP
        e.g. [5, 10, 15, 20]
    """

    ref_dataset_file = ref_val_files[0]
    val_dataset_file = ref_val_files[1]

    for ratio in ratio_E_F:
        for sigma in sigma_E:
            folder_name = f"sigma_radio_{ratio}_{sigma:.0e}"
            default_sigma = fitting_dict['gap_params']['default_sigma']

            sigma_f = sigma / ratio
            new_sigma = [sigma, sigma_f] + default_sigma[2:]

            fitting_dict['gap_params']['default_sigma'] = new_sigma

            if os.path.exists(folder_name):
                os.chdir(folder_name)
            else:
                os.mkdir(folder_name)
                os.chdir(folder_name)
            
            # Copy ref_dataset_file
            os.system(f"cp ../input/{ref_dataset_file} train.xyz")
            os.system(f"cp ../input/{val_dataset_file} validation.xyz")
            os.system(f"cp ../input/test.xyz .")
            os.system(f"cp ../input/run.py .")
            os.system(f"cp ../input/gnuparallel.sh .")

            # write new fitting_dict to file
            with open("multistage_gap_parameters.json", "w") as f:
                json.dump(fitting_dict, f, indent=4)

            #run_command = "python run.py -gap train.xyz -val train.xyz validation.xyz -err validation_gap.xyz train_gap.xyz > py.out"
            run_gapfit     = "python run.py -gap train.xyz > py.out; "
            run_eval_train = "python run.py -gnu train.xyz 40; mv task.log task_train.log; "
            run_eval_val   = "python run.py -gnu validation.xyz 40; mv task.log task_val.log; "
            run_eval_test  = "python run.py -gnu test.xyz 40; mv task.log task_test.log; "
            run_err        = "python run.py -err validation_gap.xyz train_gap.xyz test_gap.xyz >> py.out"

            run_command    = run_gapfit + run_eval_train + run_eval_val + run_eval_test + run_err
            # Write submit.sh ans submit job
            # raven_submit(time="04:00:00", disk_space=256000, job_name=f"gap-fit", run_command=run_command)
            jobs_submit(time=submit_dict['time'],
                 disk_space=submit_dict['disk_space'],
                 job_name=submit_dict['job_name'],
                 partition=submit_dict['partition'],
                 core=submit_dict['core'],
                 mpcdf=submit_dict['mpcdf'],
                 run_command=run_command)
            os.system(f"sbatch submit.sh")
            
            os.chdir("..")

    return 0

def kcal2ev(kcal:list):
    return np.array(kcal) * 0.0433641153088

def plot_mesh_evaluation(dataset_files:list, property_prefixs:list=['dft_', 'gap_']):
    """
    Plot vs for mesh evaluation results
    """

    if len(dataset_files) == 1:
        ref_dataset_file, val_dataset_file = dataset_files[0], dataset_files[0]
    elif len(dataset_files) == 2:
        ref_dataset_file, val_dataset_file = dataset_files
   
    ref_property_prefix, gap_property_prefix = property_prefixs
    ref_property = [ref_property_prefix + 'energy', ref_property_prefix + 'forces']
    val_property = [gap_property_prefix + 'energy', gap_property_prefix + 'forces']

    ref_dataset = ase.io.read(ref_dataset_file, ':')
    val_dataset = ase.io.read(val_dataset_file, ':')

    # Calculate energy per atom
    ref_energy_per_atom = [atoms.info[ref_property[0]] / atoms.get_global_number_of_atoms() for atoms in ref_dataset]
    val_energy_per_atom = [atoms.info[val_property[0]] / atoms.get_global_number_of_atoms() for atoms in val_dataset]
    ref_energy_per_atom = kcal2ev(ref_energy_per_atom)
    val_energy_per_atom = kcal2ev(val_energy_per_atom)


    # Calculate force per atom
    ref_forces, val_forces = [], []
    [ref_forces.extend(atoms.get_array(ref_property[1]).flatten()) for atoms in ref_dataset]
    [val_forces.extend(atoms.get_array(val_property[1]).flatten()) for atoms in val_dataset]
    ref_forces = kcal2ev(ref_forces)
    val_forces = kcal2ev(val_forces)


    # Calculate RMSE and MAE
    mae_energy = np.mean(np.abs(np.array(ref_energy_per_atom) - np.array(val_energy_per_atom)))
    rmse_energy = np.sqrt(np.mean((np.array(ref_energy_per_atom) - np.array(val_energy_per_atom))**2))


    mae_forces = np.mean(np.abs(np.array(ref_forces) - np.array(val_forces)))
    rmse_forces = np.sqrt(np.mean((np.array(ref_forces) - np.array(val_forces))**2))


    print(f"Energy: MAE = {mae_energy:.4f} eV/atom, RMSE = {rmse_energy:.4f} eV/atom")
    print(f"Forces: MAE = {mae_forces:.4f} eV/Å, RMSE = {rmse_forces:.4f} eV/Å")

    fig, ax = plt.subplots(1, 2, figsize=(10, 4))
    import matplotlib.colors as colorss
    
    # Create a 2D histogram
    n_bins = int(len(ref_dataset)/30)
    hist, xedges, yedges = np.histogram2d(ref_energy_per_atom, val_energy_per_atom, bins=[n_bins, n_bins])

    # Mask zeros to make them white
    hist /= np.sum(hist)
    hist = np.ma.masked_where(hist == 0, hist)
    # Plotting
    print(np.mean(hist), np.min(hist))
    ax0 = ax[0].pcolormesh(xedges, yedges, hist.T, cmap='Reds', norm=colorss.LogNorm(vmin=0.00001))#np.max(hist))
    ax[0].plot(ref_energy_per_atom, ref_energy_per_atom, c='k', alpha=0.5)
    ax[0].set_xlabel('DFT energy (eV/atom)')
    ax[0].set_ylabel('GAP energy (eV/atom)')

    hist, xedges, yedges = np.histogram2d(ref_forces, val_forces, bins=[n_bins, n_bins])
    # Mask zeros to make them white
    hist = np.ma.masked_where(hist == 0, hist)
    hist /= np.sum(hist)
    # Plotting
    ax1 = ax[1].pcolormesh(xedges, yedges, hist.T, cmap='Reds', norm=colorss.LogNorm(vmin=0.00001))
    ax[1].plot(ref_forces, ref_forces, c='k', alpha=0.5)
    ax[1].set_xlabel('DFT energy (eV/atom)')
    ax[1].set_ylabel('GAP energy (eV/atom)')

    
    # Add colorbar
    # Add space for the colorbar
    fig.subplots_adjust(right=0.9)
    cbar_ax = fig.add_axes([0.85, 0.2, 0.02, 0.74])
    cbar = plt.colorbar(ax1, cax=cbar_ax, ax=[ax0,ax1], label='Density')
    cbar.ax.minorticks_off()


    # Anotate MAE and RMSE
    ax[0].annotate(f"MAE = {mae_energy:.4f} eV/atom\nRMSE = {rmse_energy:.4f} eV/atom", xy=(0.05, 0.8), xycoords='axes fraction')
    # Anotate MAE and RMSE
    ax[1].annotate(f"MAE = {mae_forces:.4f} eV/Å\nRMSE = {rmse_forces:.4f} eV/Å", xy=(0.05, 0.8), xycoords='axes fraction')
    ax[1].set_xlabel('DFT forces (eV/Å)')
    ax[1].set_ylabel('GAP forces (eV/Å)')
    plt.tight_layout(rect=[0, 0, 0.85, 1])

    fig.savefig('simple_vs.png', dpi=200)
    print("Plot saved as simple_vs.png")

def eV2meV(data_eV:list):
    for data in data_eV:
        data *= 1000
    return data_eV


def plot_evaluation1(dataset_files:list, property_prefixs:list=['dft_', 'gap_']):
    """
    Plot evaluation results

    Parameters:
    ----------
    dataset_files(list): list of reference and validation dataset files
        e.g. ["train.xyz", "validation.xyz"]

    property_prefixs(list): list of property prefix for reference and GAP
        e.g. ['dft_', 'gap_']
    """

    if len(dataset_files) == 1:
        ref_dataset_file, val_dataset_file = dataset_files[0], dataset_files[0]
        fig_outfile = f"fig_{ref_dataset_file.split('/')[-1].replace('.xyz', '.png')}"
    elif len(dataset_files) == 2:
        ref_dataset_file, val_dataset_file = dataset_files
    
    ref_property_prefix, gap_property_prefix = property_prefixs
    ref_property = [ref_property_prefix + 'energy', ref_property_prefix + 'forces']
    val_property = [gap_property_prefix + 'energy', gap_property_prefix + 'forces']

    ref_dataset = ase.io.read(ref_dataset_file, ':')
    val_dataset = ase.io.read(val_dataset_file, ':')


    # Calculate energy per atom
    ref_energy_per_atom = [atoms.info[ref_property[0]] / atoms.get_global_number_of_atoms() for atoms in ref_dataset]
    val_energy_per_atom = [atoms.info[val_property[0]] / atoms.get_global_number_of_atoms() for atoms in val_dataset]
    ref_energy_per_atom = kcal2ev(ref_energy_per_atom)
    val_energy_per_atom = kcal2ev(val_energy_per_atom)


    # Calculate force
    ref_forces, val_forces = [], []
    [ref_forces.extend(atoms.get_array(ref_property[1]).flatten()) for atoms in ref_dataset]
    [val_forces.extend(atoms.get_array(val_property[1]).flatten()) for atoms in val_dataset]
    ref_forces = kcal2ev(ref_forces)
    val_forces = kcal2ev(val_forces)


    # eV to meV
    ref_energy_per_atom, val_energy_per_atom, ref_forces, val_forces = eV2meV([ref_energy_per_atom, val_energy_per_atom, ref_forces, val_forces])


    # Calculate RMSE and MAE
    mae_energy = np.mean(np.abs(np.array(ref_energy_per_atom) - np.array(val_energy_per_atom)))
    rmse_energy = np.sqrt(np.mean((np.array(ref_energy_per_atom) - np.array(val_energy_per_atom))**2))


    mae_forces = np.mean(np.abs(np.array(ref_forces) - np.array(val_forces)))
    rmse_forces = np.sqrt(np.mean((np.array(ref_forces) - np.array(val_forces))**2))


    print(f"Energy: MAE = {mae_energy:.2f} meV/atom, RMSE = {rmse_energy:.2f} meV/atom")
    print(f"Forces: MAE = {mae_forces:.2f} meV/Å, RMSE = {rmse_forces:.2f} meV/Å")

    fig, ax = plt.subplots(1, 2, figsize=(5, 4))
    xy = np.linspace(min(ref_energy_per_atom), max(ref_energy_per_atom), 100)
    ax[0].scatter(ref_energy_per_atom, val_energy_per_atom, c=colors[1], alpha=0.2)
    ax[0].plot(xy,xy, c='k')
    ax[0].set_ylim(min(ref_energy_per_atom), max(ref_energy_per_atom))
    ax[0].set_xlim(min(ref_energy_per_atom), max(ref_energy_per_atom))
    # Anotate MAE and RMSE
    # ax[0].annotate(f"MAE = {mae_energy:.2f} meV/atom\nRMSE = {rmse_energy:.2f} meV/atom", xy=(0.02, 0.8), xycoords='axes fraction')
    ax[0].annotate(f"MAE = {mae_energy:.2f}\nRMSE = {rmse_energy:.2f}", xy=(0.02, 0.85), xycoords='axes fraction')

    # ax[0].set_xlabel('DFT energy (meV/atom')
    # ax[0].set_ylabel('GAP energy (meV/atom)')

    ax[0].set_ylabel('GAP')
    ax[0].set_title('Energy (meV/atom)')

    xy = np.linspace(min(ref_forces), max(ref_forces), 100)
    ax[1].scatter(ref_forces, val_forces, c=colors[1], alpha=0.2)
    ax[1].plot(xy, xy, c='k')
    ax[1].set_ylim(min(ref_forces), max(ref_forces))
    ax[1].set_xlim(min(ref_forces), max(ref_forces))
    # set log to y ticks
    # ax[1].set_yscale('log')
    # ax[1].set_xscale('log')
    # Anotate MAE and RMSE
    ax[1].annotate(f"MAE = {mae_forces:.2f}\nRMSE = {rmse_forces:.2f}", xy=(0.02, 0.85), xycoords='axes fraction')
    # ax[1].set_xlabel('DFT forces (meV/Å)')
    # ax[1].set_ylabel('GAP forces (meV/Å)')
    ax[1].set_title('Forces (meV/Å)')
    fig.text(0.6, 0.03, 'DFT', ha='center', va='center')

    # set super xlabel to the figure
    plt.tight_layout()
    fig.savefig(fig_outfile, dpi=200)
    print(f"Plot saved as {fig_outfile}")


def plot_evaluation(dataset_files:list, property_prefixs:list=['dft_', 'gap_']):
    """
    Plot evaluation results

    Parameters:
    ----------
    dataset_files(list): list of reference and validation dataset files
        e.g. ["train.xyz", "validation.xyz"]

    property_prefixs(list): list of property prefix for reference and GAP
        e.g. ['dft_', 'gap_']
    """

    if len(dataset_files) == 1:
        ref_dataset_file, val_dataset_file = dataset_files[0], dataset_files[0]
        fig_outfile = f"fig_{ref_dataset_file.split('/')[-1].replace('.xyz', '.png')}"
    elif len(dataset_files) == 2:
        ref_dataset_file, val_dataset_file = dataset_files
    
    ref_property_prefix, gap_property_prefix = property_prefixs
    ref_property = [ref_property_prefix + 'energy', ref_property_prefix + 'forces']
    val_property = [gap_property_prefix + 'energy', gap_property_prefix + 'forces']

    ref_dataset = ase.io.read(ref_dataset_file, ':')
    val_dataset = ase.io.read(val_dataset_file, ':')


    # Calculate energy per atom
    ref_energy_per_atom = [atoms.info[ref_property[0]] / atoms.get_global_number_of_atoms() for atoms in ref_dataset]
    val_energy_per_atom = [atoms.info[val_property[0]] / atoms.get_global_number_of_atoms() for atoms in val_dataset]
    ref_energy_per_atom = kcal2ev(ref_energy_per_atom)
    val_energy_per_atom = kcal2ev(val_energy_per_atom)


    # Calculate force
    ref_forces, val_forces = [], []
    [ref_forces.extend(atoms.get_array(ref_property[1]).flatten()) for atoms in ref_dataset]
    [val_forces.extend(atoms.get_array(val_property[1]).flatten()) for atoms in val_dataset]
    ref_forces = kcal2ev(ref_forces)
    val_forces = kcal2ev(val_forces)


    # eV to meV
    ref_energy_per_atom, val_energy_per_atom, ref_forces, val_forces = eV2meV([ref_energy_per_atom, val_energy_per_atom, ref_forces, val_forces])


    # Calculate RMSE and MAE
    mae_energy = np.mean(np.abs(np.array(ref_energy_per_atom) - np.array(val_energy_per_atom)))
    rmse_energy = np.sqrt(np.mean((np.array(ref_energy_per_atom) - np.array(val_energy_per_atom))**2))


    mae_forces = np.mean(np.abs(np.array(ref_forces) - np.array(val_forces)))
    rmse_forces = np.sqrt(np.mean((np.array(ref_forces) - np.array(val_forces))**2))


    print(f"Energy: MAE = {mae_energy:.2f} meV/atom, RMSE = {rmse_energy:.2f} meV/atom")
    print(f"Forces: MAE = {mae_forces:.2f} meV/Å, RMSE = {rmse_forces:.2f} meV/Å")

    fig, ax = plt.subplots(1, 2, figsize=(9, 4))
    ax[0].scatter(ref_energy_per_atom, val_energy_per_atom, c=colors[1], alpha=0.2)
    ax[0].plot(ref_energy_per_atom, ref_energy_per_atom, c='k')
    # Anotate MAE and RMSE
    ax[0].annotate(f"MAE = {mae_energy:.2f} meV/atom\nRMSE = {rmse_energy:.2f} meV/atom", xy=(0.02, 0.8), xycoords='axes fraction')
    ax[0].set_xlabel('DFT energy (meV/atom)')
    ax[0].set_ylabel('GAP energy (meV/atom)')

    ax[1].scatter(ref_forces, val_forces, c=colors[1], alpha=0.2)
    ax[1].plot(ref_forces, ref_forces, c='k')
    # Anotate MAE and RMSE
    ax[1].annotate(f"MAE = {mae_forces:.2f} meV/Å\nRMSE = {rmse_forces:.2f} meV/Å", xy=(0.02, 0.8), xycoords='axes fraction')
    ax[1].set_xlabel('DFT forces (meV/Å)')
    ax[1].set_ylabel('GAP forces (meV/Å)')
    plt.tight_layout()

    fig.savefig(fig_outfile, dpi=200)
    print(f"Plot saved as {fig_outfile}")



def get_vs_errors(folders:list):

    errors_vs = {}
    for folder in folders:
        json_file = f"{folder}/error.json"
        with open(json_file, "r") as f:
            error = json.load(f)
        
        errors_vs[folder] = error
    
    with open("errors_vs.json", 'w') as f:
        json.dump(errors_vs, f, indent=4)
    
    return errors_vs

def get_mae_rmse(folders:list, errors_vs:dict):

    # Get MAE for train and validation
    mae_train_energy, mae_val_energy, mae_train_forces, mae_val_forces = [], [], [], []
    
    rmse_train_energy, rmse_val_energy, rmse_train_forces, rmse_val_forces = [], [], [], []

    for folder in folders:
        err = errors_vs[folder]
        mae_train_energy.append(err['train']['energy']['MAE'])
        mae_val_energy.append(err['validation']['energy']['MAE'])
        mae_train_forces.append(err['train']['forces']['MAE'])
        mae_val_forces.append(err['validation']['forces']['MAE'])

        rmse_train_energy.append(err['train']['energy']['RMSE'])
        rmse_val_energy.append(err['validation']['energy']['RMSE'])
        rmse_train_forces.append(err['train']['forces']['RMSE'])
        rmse_val_forces.append(err['validation']['forces']['RMSE'])
    
    return mae_train_energy, mae_val_energy, mae_train_forces, mae_val_forces, rmse_train_energy, rmse_val_energy, rmse_train_forces, rmse_val_forces


def plot_nsparse_vs(folders:list, tag, errors_vs_file:str=None):

    if errors_vs_file is None:
        errors_vs = get_vs_errors(folders)
    else:
        with open(errors_vs_file, 'r') as f:
            errors_vs = json.load(f)
    

    mae_train_energy, mae_val_energy, mae_train_forces, mae_val_forces, rmse_train_energy, rmse_val_energy, rmse_train_forces, rmse_val_forces = get_mae_rmse(folders, errors_vs)

    x_labels = [folder.split('_')[-1] for folder in folders]

    alpha = 0.75
    fig, ax = plt.subplots(2,2, figsize=(10,8), dpi=150)
    ax[0][0].plot(x_labels, mae_train_energy, label='Trainning', marker=markers[0], color=colors[0], linestyle=linestyles[0], alpha=alpha)
    ax[0][0].plot(x_labels, mae_val_energy, label='Validation', marker=markers[1], color=colors[2], linestyle=linestyles[0], alpha=alpha)

    ax[0][0].set_ylabel('MAE Energy (eV/atom)')
    # rotate xticklabels 45 degree
    ax[0][0].set_xticklabels(x_labels, rotation=45)
    # ax[0][0].set_xlabel(f"{tag}")
    ax[0][0].legend()


    ax[0][1].plot(x_labels, mae_train_forces, label='Trainning', marker=markers[0], color=colors[0], linestyle=linestyles[0], alpha=alpha)
    ax[0][1].plot(x_labels, mae_val_forces, label='Vlidation', marker=markers[1], color=colors[2], linestyle=linestyles[0], alpha=alpha)

    ax[0][1].set_ylabel('MAE Forces (eV/Å)')
    # ax[0][1].set_xlabel(f"{tag}")
    ax[0][1].set_xticklabels(x_labels, rotation=45)

    ax[0][1].legend()



    ax[1][0].plot(x_labels, rmse_train_energy, label='Training', marker=markers[0], color=colors[0], linestyle=linestyles[1], alpha=alpha)
    ax[1][0].plot(x_labels, rmse_val_energy, label='Validation', marker=markers[1], color=colors[2], linestyle=linestyles[1], alpha=alpha)

    ax[1][0].set_ylabel('RMSE Energy (eV/atom)')
    # ax[1][0].set_xlabel(f"{tag}")
    ax[1][0].set_xticklabels(x_labels, rotation=45)
    ax[1][0].legend()


    ax[1][1].plot(x_labels, rmse_train_forces, label='RMSE(train)', marker=markers[0], color=colors[0], linestyle=linestyles[1], alpha=alpha)
    ax[1][1].plot(x_labels, rmse_val_forces, label='RMSE(val)', marker=markers[1], color=colors[2], linestyle=linestyles[1], alpha=alpha)

    ax[1][1].set_ylabel('RMSE Forces (eV/Å)')
    # ax[1][1].set_xlabel(f"{tag}")
    ax[1][1].set_xticklabels(x_labels, rotation=45)
    ax[1][1].legend()


    fig.tight_layout()
    fig.savefig('hyperparameter_vs.png', dpi=400)     

    return 0    



def plot_nlmax_vs(folders:list, n_maxs:list, l_maxs:list, tag:str, errors_vs_file:str=None):

    if errors_vs_file is None:
        errors_vs = get_vs_errors(folders)
    else:
        with open(errors_vs_file, 'r') as f:
            errors_vs = json.load(f)


    len_n, len_l = len(n_maxs), len(l_maxs)

    mae_train_energy = [[0] * len_n for _ in range(len_l)]
    mae_val_energy = [[0] * len_n for _ in range(len_l)]
    mae_train_forces = [[0] * len_n for _ in range(len_l)]
    mae_val_forces = [[0] * len_n for _ in range(len_l)]

    rmse_train_energy = [[0] * len_n for _ in range(len_l)]
    rmse_val_energy = [[0] * len_n for _ in range(len_l)]
    rmse_train_forces = [[0] * len_n for _ in range(len_l)]
    rmse_val_forces = [[0] * len_n for _ in range(len_l)]

    for l, l_max in enumerate(l_maxs):
        for n, n_max in enumerate(n_maxs):
            folder = f"nlmax_{n_max}_{l_max}"
            mae_val_energy[l][n] = errors_vs[folder]['validation']['energy']['MAE']
            mae_train_energy[l][n] = errors_vs[folder]['train']['energy']['MAE']
            mae_val_forces[l][n] = errors_vs[folder]['validation']['forces']['MAE']
            mae_train_forces[l][n] = errors_vs[folder]['train']['forces']['MAE']

            rmse_val_energy[l][n] = errors_vs[folder]['validation']['energy']['RMSE']
            rmse_train_energy[l][n] = errors_vs[folder]['train']['energy']['RMSE']
            rmse_val_forces[l][n] = errors_vs[folder]['validation']['forces']['RMSE']
            rmse_train_forces[l][n] = errors_vs[folder]['train']['forces']['RMSE']
    
        # Plot MAE
    mae_val_energy = np.array(mae_val_energy) * 1000
    mae_train_energy = np.array(mae_train_energy) * 1000
    mae_val_forces = np.array(mae_val_forces) * 1000
    mae_train_forces = np.array(mae_train_forces) * 1000

    rmse_val_energy = np.array(rmse_val_energy) * 1000
    rmse_train_energy = np.array(rmse_train_energy) * 1000
    rmse_val_forces = np.array(rmse_val_forces) * 1000
    rmse_train_forces = np.array(rmse_train_forces) * 1000


    fig, ax = plt.subplots(2,2, figsize=(10,8), dpi=150)
    ax[0][0].imshow(mae_train_energy, cmap='YlGn')
    ax[0][0].set_xticks(np.arange(len(n_maxs)), labels=n_maxs)
    ax[0][0].set_yticks(np.arange(len(l_maxs)), labels=l_maxs)
    ax[0][0].set_xlabel('n_max')
    ax[0][0].set_ylabel('l_max')
    ax[0][0].set_title('Training MAE Energy (eV/atom)')

    im0 = ax[0][1].imshow(mae_val_energy, cmap='YlGn')
    ax[0][1].set_xticks(np.arange(len(n_maxs)), labels=n_maxs)
    ax[0][1].set_yticks(np.arange(len(l_maxs)), labels=l_maxs)
    ax[0][1].set_xlabel('n_max')
    ax[0][1].set_ylabel('l_max')
    ax[0][1].set_title('Validation MAE Energy (eV/atom)')

    ax[1][0].imshow(mae_train_forces, cmap='YlGn')
    ax[1][0].set_xticks(np.arange(len(n_maxs)), labels=n_maxs)
    ax[1][0].set_yticks(np.arange(len(l_maxs)), labels=l_maxs)
    ax[1][0].set_xlabel('n_max')
    ax[1][0].set_ylabel('l_max')

    ax[1][0].set_title('Training MAE Forces (eV/Å)')
    im1 = ax[1][1].imshow(mae_val_forces, cmap='YlGn')
    ax[1][1].set_xticks(np.arange(len(n_maxs)), labels=n_maxs)
    ax[1][1].set_yticks(np.arange(len(l_maxs)), labels=l_maxs)
    ax[1][1].set_xlabel('n_max')
    ax[1][1].set_ylabel('l_max')
    ax[1][1].set_title('Validation MAE Forces (eV/Å)')
    # add color bar

    cbar0 = fig.colorbar(im0, ax=ax[0][1], shrink=0.95)
    cbar0.ax.set_ylabel('MAE Energy (eV/atom)')

    cbar1 = fig.colorbar(im1, ax=ax[1][1], shrink=0.95)
    cbar1.ax.set_ylabel('MAE Forces (eV/Å)')
    
    fig.tight_layout()
    fig.savefig('hyperparameter_vs.png', dpi=400)


    return 0


def plot_sigma_vs(folders:list, sigma_E, ratio_E_F, tag:str, errors_vs_file:str=None):

    if errors_vs_file is None:
        errors_vs = get_vs_errors(folders)
    else:
        with open(errors_vs_file, 'r') as f:
            errors_vs = json.load(f)


    len_sigma, len_ratio = len(sigma_E), len(ratio_E_F)

    mae_train = [[0] * len_sigma for _ in range(len_ratio)]
    mae_val = [[0] * len_sigma for _ in range(len_ratio)]

    rmse_train = [[0] * len_sigma for _ in range(len_ratio)]
    rmse_val = [[0] * len_sigma for _ in range(len_ratio)]

    for r, ratio in enumerate(ratio_E_F):
        for s, sigma in enumerate(sigma_E):
            folder = f"sigma_radio_{ratio}_{sigma:.0e}"
            mae_val[r][s] = errors_vs[folder]['validation']['LOSS']['MAE']
            mae_train[r][s] = errors_vs[folder]['train']['LOSS']['MAE']
    
            rmse_val[r][s] = errors_vs[folder]['validation']['LOSS']['RMSE']
            rmse_train[r][s] = errors_vs[folder]['train']['LOSS']['RMSE']
    
    # Plot MAE
    fig, ax = plt.subplots(2,2, figsize=(10,8), dpi=150)
    for r, ratio in enumerate(ratio_E_F):
        ax[0][0].plot(sigma_E, mae_train[r][:], color=colors[r], linestyle=linestyles[r], label=f"$\sigma_E/\sigma_F$ = {ratio}")
    ax[0][0].set_xscale('log')
    ax[0][0].set_xlabel('$\sigma_\mathrm{Energy}$')
    ax[0][0].set_ylabel('$MAE_\mathrm{Energy} + 0.1 MAE_\mathrm{Forces}$')
    ax[0][0].legend()
    

    for r, ratio in enumerate(ratio_E_F):
        ax[0][1].plot(sigma_E, mae_val[r][:], color=colors[r], linestyle=linestyles[r], label=f"$\sigma_E/\sigma_F$ = {ratio}")
    ax[0][1].set_xscale('log')
    ax[0][1].set_xlabel('$\sigma_\mathrm{Energy}$')
    ax[0][1].set_ylabel('$MAE_\mathrm{Energy} + 0.1 MAE_\mathrm{Forces}$')
    ax[0][1].legend()
    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][0].plot(sigma_E, rmse_train[r][:], color=colors[r], linestyle=linestyles[r], label=f"$\sigma_E/\sigma_F$ = {ratio}")
    ax[1][0].set_xscale('log')
    ax[1][0].set_xlabel('$\sigma_\mathrm{Energy}$')
    ax[1][0].set_ylabel('$RMSE_\mathrm{Energy} + 0.1 RMSE_\mathrm{Forces}$')
    ax[1][0].legend()

    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][1].plot(sigma_E, rmse_val[r][:], color=colors[r], linestyle=linestyles[r], label=f"$\sigma_E/\sigma_F$ = {ratio}")
    ax[1][1].set_xscale('log')
    ax[1][1].set_xlabel('$\sigma_\mathrm{Energy}$')
    ax[1][1].set_ylabel('$RMSE_\mathrm{Energy} + 0.1 RMSE_\mathrm{Forces}$')
    ax[1][1].legend()

    fig.tight_layout()
    fig.savefig('hyperparameter_vs.png', dpi=400)

    return 0

def plot_nlAsigma_vs(folders:list, sigma_E, ratio_E_F, tag:str, errors_vs_file:str=None):

    if errors_vs_file is None:
        errors_vs = get_vs_errors(folders)
    else:
        with open(errors_vs_file, 'r') as f:
            errors_vs = json.load(f)


    len_sigma, len_ratio = len(sigma_E), len(ratio_E_F)

    mae_train_energy = [[0] * len_sigma for _ in range(len_ratio)]
    mae_val_energy = [[0] * len_sigma for _ in range(len_ratio)]
    mae_train_forces = [[0] * len_sigma for _ in range(len_ratio)]
    mae_val_forces = [[0] * len_sigma for _ in range(len_ratio)]

    rmse_train_energy = [[0] * len_sigma for _ in range(len_ratio)]
    rmse_val_energy = [[0] * len_sigma for _ in range(len_ratio)]
    rmse_train_forces = [[0] * len_sigma for _ in range(len_ratio)]
    rmse_val_forces = [[0] * len_sigma for _ in range(len_ratio)]

    for r, ratio in enumerate(ratio_E_F):
        for s, sigma in enumerate(sigma_E):
            if tag == "nlmax":
                folder = f"nlmax_{sigma}_{ratio}"
            elif tag == "sigma":
                folder = f"sigma_radio_{ratio}_{sigma:.0e}"
            # mae_val[r][s] = errors_vs[folder]['validation']['LOSS']['MAE']
            # mae_train[r][s] = errors_vs[folder]['train']['LOSS']['MAE']
    
            # rmse_val[r][s] = errors_vs[folder]['validation']['LOSS']['RMSE']
            # rmse_train[r][s] = errors_vs[folder]['train']['LOSS']['RMSE']
            mae_val_energy[r][s] = errors_vs[folder]['validation']['energy']['MAE']
            mae_train_energy[r][s] = errors_vs[folder]['train']['energy']['MAE']
            mae_val_forces[r][s] = errors_vs[folder]['validation']['forces']['MAE']
            mae_train_forces[r][s] = errors_vs[folder]['train']['forces']['MAE']

            rmse_val_energy[r][s] = errors_vs[folder]['validation']['energy']['RMSE']
            rmse_train_energy[r][s] = errors_vs[folder]['train']['energy']['RMSE']
            rmse_val_forces[r][s] = errors_vs[folder]['validation']['forces']['RMSE']
            rmse_train_forces[r][s] = errors_vs[folder]['train']['forces']['RMSE']

    
    # Plot MAE
    fig, ax = plt.subplots(2,2, figsize=(10,8), dpi=150)


    if tag == "sigma":
        ax[0][0].set_xscale('log')
        ax[0][0].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[0][1].set_xscale('log')
        ax[0][1].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[1][0].set_xscale('log')
        ax[1][0].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[1][1].set_xscale('log')
        ax[1][1].set_xlabel('$\sigma_\mathrm{Energy}$')
        lab = '$\sigma_E/\sigma_F$'
    elif tag == "nlmax":
        ax[0][0].set_xlabel('$n_\mathrm{max}$')
        ax[0][1].set_xlabel('$n_\mathrm{max}$')
        ax[1][0].set_xlabel('$n_\mathrm{max}$')
        ax[1][1].set_xlabel('$n_\mathrm{max}$')
        lab = '$l_\mathrm{max}$'

    from matplotlib.ticker import ScalarFormatter
    for r, ratio in enumerate(ratio_E_F):
        ax[0][0].plot(sigma_E, mae_train_energy[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[0][0].set_ylabel('$\mathrm{MAE}_\mathrm{Energy}$')
    ax[0][0].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[0][0].set_title("Training")
    ax[0][0].legend()
    

    for r, ratio in enumerate(ratio_E_F):
        ax[0][1].plot(sigma_E, mae_train_forces[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[0][1].set_ylabel('$\mathrm{MAE}_\mathrm{Forces}$')
    ax[0][1].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[0][1].set_title("Training")
    ax[0][1].legend()
    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][0].plot(sigma_E, mae_val_energy[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[1][0].set_ylabel('$\mathrm{MAE}_\mathrm{Energy}$')
    ax[1][0].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[1][0].set_title("Validation")
    ax[1][0].legend()

    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][1].plot(sigma_E, mae_val_forces[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[1][1].set_ylabel('$\mathrm{MAE}_\mathrm{Forces}$')
    ax[1][1].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[1][1].set_title("Validation")
    ax[1][1].legend()

    
    fig.tight_layout()
    fig.savefig('hyperparameter_mae_vs.png', dpi=400)

    fig.clf()
    fig, ax = plt.subplots(2,2, figsize=(10,8), dpi=150)


    if tag == "sigma":
        ax[0][0].set_xscale('log')
        ax[0][0].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[0][1].set_xscale('log')
        ax[0][1].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[1][0].set_xscale('log')
        ax[1][0].set_xlabel('$\sigma_\mathrm{Energy}$')
        ax[1][1].set_xscale('log')
        ax[1][1].set_xlabel('$\sigma_\mathrm{Energy}$')
        lab = '$\sigma_E/\sigma_F$'
    elif tag == "nlmax":
        ax[0][0].set_xlabel('$n_\mathrm{max}$')
        ax[0][1].set_xlabel('$n_\mathrm{max}$')
        ax[1][0].set_xlabel('$n_\mathrm{max}$')
        ax[1][1].set_xlabel('$n_\mathrm{max}$')
        lab = '$l_\mathrm{max}$'

    from matplotlib.ticker import ScalarFormatter
    for r, ratio in enumerate(ratio_E_F):
        ax[0][0].plot(sigma_E, rmse_train_energy[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[0][0].set_ylabel('$\mathrm{RMSE}_\mathrm{Energy}$')
    ax[0][0].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[0][0].set_title("Training")
    ax[0][0].legend()
    

    for r, ratio in enumerate(ratio_E_F):
        ax[0][1].plot(sigma_E, rmse_train_forces[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[0][1].set_ylabel('$\mathrm{RMSE}_\mathrm{Forces}$')
    ax[0][1].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[0][1].set_title("Training")
    ax[0][1].legend()
    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][0].plot(sigma_E, rmse_val_energy[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[1][0].set_ylabel('$\mathrm{RMSE}_\mathrm{Energy}$')
    ax[1][0].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[1][0].set_title("Validation")
    ax[1][0].legend()

    
    for r, ratio in enumerate(ratio_E_F):
        ax[1][1].plot(sigma_E, rmse_val_forces[r][:], color=colors[r], linestyle=linestyles[r], label=f"{lab} = {ratio}")
    ax[1][1].set_ylabel('$\mathrm{RMSE}_\mathrm{Forces}$')
    ax[1][1].yaxis.set_major_formatter(ScalarFormatter(useOffset=False, useMathText=False))
    ax[1][1].set_title("Validation")
    ax[1][1].legend()

    
    fig.tight_layout()
    fig.savefig('hyperparameter_rmse_vs.png', dpi=400)

    return 0


# def plot(folders:list, )

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Hyperparameter selection for GAP')
    argparser.add_argument('-b2_cutoff', '--b2_cutoff', action='store_true',
                            help='whether to do 2-body hyperparameter selection or not')
    
    argparser.add_argument('-b2_nsparse', '--b2_nsparse', action='store_true',
                            help='whether to do 2-body hyperparameter selection or not')
    
    argparser.add_argument('-soap_nl', '--soap_nlmax', action='store_true',
                            help='search n_max and l_max for SOAP')
    
    argparser.add_argument('-soap_nsparse', '--soap_nsparse', action='store_true',
                            help='search n_max and l_max for SOAP')
    
    argparser.add_argument('-sigma', '--sigma_e_f', action='store_true',
                            help='search sigma for energy and forces')
    
    argparser.add_argument('-plot_vs', '--plot_evaluation', nargs='+', type=str,
                            help='plot evaluation results')
    
    argparser.add_argument('-plot_hp_vs', '--plot_hyperparameter_vs', nargs='+', type=str,
                            help='plot hyperparameter vs MAE and RMSE, b2_nsparse, soap_nsparse, nlmax, sigma')                          
    
    argparser.add_argument('-mpcdf', '--mpcdf',
                    default=None,
                    type=str,
                    help='raven or cobra')
    
args = argparser.parse_args()

ref_dataset_file = "train.xyz"
val_dataset_file = "validation.xyz"
test_dataset_file = "test.xyz"

datset_files = [ref_dataset_file, val_dataset_file]

# The fitting parameters are stored in ./gap_fit_parameters.json
b2_nsparses = [3, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100]
soap_nsparses = [50, 100, 500, 1000, 2000, 2500, 3000, 4000, 5000]
n_maxs = [4, 5, 6, 8, 9, 10]
l_maxs = [2, 3, 4, 5]
sigma_E = [10e-5, 10e-4, 10e-3, 10e-2, 10e-1]
ratio_E_F = [5, 10, 20, 50]


submit_dict = {
    'time':"19:00:00",
    'disk_space':90000,
    'job_name':"DFTB",
    'mpcdf': args.mpcdf
}

if args.mpcdf == 'raven':
    submit_dict['partition'] = "general"
    submit_dict['core'] = 72

elif args.mpcdf == 'cobra':
    submit_dict['partition'] = "middle"
    submit_dict['core'] = 40
else:
    print("Please set -mpcdf is raven or cobra!")


if args.b2_cutoff:
    cutoffs = [4.5, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]

    with open("./input/multistage0_gap_parameters.json", "r") as stream:
        fitting_dict = json.load(stream)

    vary_2b_cutoff(datset_files, fitting_dict, cutoffs)

if args.b2_nsparse:

    with open("./input/multistage0_gap_parameters.json", "r") as stream:
        fitting_dict = json.load(stream)
    
    vary_nsparse(datset_files, fitting_dict, b2_nsparses, '2b')
    # vary_2b_delta_and_sparse(fitting_dict, deltas, sparses)
if args.soap_nsparse:

    with open("./input/multistage_gap_parameters.json", "r") as stream:
        fitting_dict = json.load(stream)
    
    vary_nsparse([ref_dataset_file, val_dataset_file], fitting_dict, soap_nsparses, 'soap')

if args.soap_nlmax:

    with open("./input/multistage_gap_parameters.json", "r") as stream:
        fitting_dict = json.load(stream)

    vary_nl_max(datset_files, fitting_dict, n_maxs, l_maxs)
        

if args.sigma_e_f:

    with open("./input/multistage_gap_parameters.json", "r") as stream:
        fitting_dict = json.load(stream)
    
    vary_sigma([ref_dataset_file, val_dataset_file], fitting_dict, sigma_E, ratio_E_F)


if args.plot_evaluation:
    dataset_files = args.plot_evaluation
    plot_evaluation1(dataset_files, ['dft_', 'gap_'])
    # plot_mesh_evaluation(dataset_files, ['dft_', 'gap_'])


if args.plot_hyperparameter_vs:

    if len(args.plot_hyperparameter_vs) == 1:
        tag = args.plot_hyperparameter_vs[0]
        errors_vs_file = None
    else:
        tag, errors_vs_file = args.plot_hyperparameter_vs

    if tag == "b2_nsparse":
        folders = [f"nsparse_{str(sparse)}" for sparse in b2_nsparses]
        plot_nsparse_vs(folders, tag, errors_vs_file=errors_vs_file)

    elif tag == "soap_nsparse":
        folders = [f"nsparse_{str(sparse)}" for sparse in soap_nsparses]
        plot_nsparse_vs(folders, tag, errors_vs_file=errors_vs_file)
    
    elif tag == "nlmax":
        folders = [f"nlmax_{n_max}_{l_max}" for n_max in n_maxs for l_max in l_maxs]
        plot_nlAsigma_vs(folders, n_maxs, l_maxs, tag, errors_vs_file=errors_vs_file)
    
    elif tag == "sigma":
        folders = [f"sigma_radio_{ratio}_{sigma:.0e}" for ratio in ratio_E_F for sigma in sigma_E]
        plot_nlAsigma_vs(folders, sigma_E, ratio_E_F, tag, errors_vs_file=errors_vs_file)
    





