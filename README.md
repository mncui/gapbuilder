# GAPBUILDER

This is the scripts collection that based on [QUIP](https://github.com/libAtoms/QUIP), [workflow](https://github.com/libAtoms/QUIP) and [universalSOPA](https://github.com/libAtoms/universalSOAP) for the idea of hyperparmeter selection.

- `hp_selection.py`: the hyperparameter selection script that designed for N sparse(2b/SOAP), $n_{max}/l_{max}$ and $\sigma_E/\sigma_F$.

- `input`: the input folder for dataset, hyperparameter files, and gap fit, et al.
    - `run.py`: The **GAP FITTING** code, the collection of gap fit, evaluate structrues, calculate error(MAE/RMSE) and plot figures
    - `train.xyz, validation.xyz and test.xyz`: the dataset 
    - `gnuparallel.sh`: used to run finished gap model, obtain the prediction for train.xyz, validation.xyz and test.xyz
    - `multistage0_gap_parameters.json`: **IMPORTANT**, the hyperparmeter for 2-body
    - `multistage_gap_parameters.json`: **IMPORTANT**, includes all hyperparameter for 2/m-body

> Pre-Preparation
- 1. Set the `jobs_submit` function in `hp_selection.py` according to your environment!
- 2. Prepare `train.xyz, validation.xyz and test.xyz` dataset before running, where we default the prefix label for energy or forces is `dft_`, which can be edited in `input/multistage_gap_parameters.json` (energy_parameter_name, forces_parameter_name), and `run.py`(ref_property_prefix, gap_property_prefix)

- 3. About the `run.py`: includes the main scripts for the gap fitting.
    - UniverSoap: Pay attention to the location of `/u/xxx/software/universalSOAP/calculations/length_scales_VASP_auto_length_scales.yaml`
    - Give basic setting for `multistage_gap_parameters.json`
    - Make sure of the property prefix: ref_property_prefix, gap_property_prefix: the prefix before "energy/forces"
    


## 1. EXAMPLES FOR GAP FITTING

The is the main script for the gap fitting which includes some basic functions like fitting, prediction, error calculating and plot a comparasion figures.
```
# This is the full way to call run.py, training, preficting, calculting error, even plot 
python run.py -gap train.xyz -mpcdf raven -val train.xyz validation.xyz -err train_gap.xyz validation_gap.xyz -plot_vs validation_gap.xyz
```

- `-gap train.xyz -mpcdf raven`: do a gat fitting based on `dft_energy` and `dft_forces` in train.xyz file

    - The `run.py` will first do a radial distribution function checking, some Descriptors (was generated automatically from UniversalSOAP) will be deleted if there is no correspondent atomic pair in the train.xyz, therefore, you will find a ![rdf.png](./input/rdf.png) under the working folder.  

- `-val train.xyz validation.xyz`: evaluate(predicted) train.xyz or validation.xyz based on the model we got (default under `./fit/GAP.xml`)
e.g.: this code will output files named `train_gap.xyz` and `validation_gap.xyz`, where energy and forces are labeled with `gap_energy and gap_forces`

- `-err train_gap.xyz validation_gap.xyz`: calculate MAE and RMSE, and store them as `error.json`
**IMPORTANT**: Making sure the prefix of energy/forces in dataset, defaultly with "dft_" and "gap_" for dft and gap(prediction), respectively.

- `-plot_vs validation_gap.xyz`: plot figure for validation.xyz.


## 2. example for hyperparmeter selection
Because we can get most of emperical hyperparmeters from `universalSOPA`, we only need to concern (1) 2-body N sparse; (2) m-body M sparse; (3) $n_{max}/l_{max}$ and (4) $\sigma_E/\sigma_F$

### 1. 2-body N sparse
```
python hp_selection.py -b2_nsparse -mpcdf raven
```

### 2. m-body M sparse
```
python hp_selection.py -soap_nsparse -mpcdf raven
```


### 3. $n_{max}/l_{max}$
```
python hp_selection.py -nlmax -mpcdf raven
```


### 4. $\sigma_E/\sigma_F$
```
python hp_selectrion.py -sigma  -mpcdf raven
```

### 3. One could also visualize the output of hyperparmeter selection
```
python hp_selection.py -plot_hp_vs xxx
```

- `xxx`
the `xxx` could be replace by `b2_nsparse`, `soap_nsparse`, `nlmax` and `sigma` to plot figure for each purpose, respectively.

eg: `python hp_selection.py -plot_hp_vs sigma`
![Example for sigma hyperparameter selection results](./input/hyperparameter_mae_vs.png)







