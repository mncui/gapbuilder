import numpy as np
import math
from ase import Atoms
import ase.io
import os
from quippy.potential import Potential
from pathlib import Path
from wfl.configset import ConfigSet, OutputSpec
from wfl.generate import md
import wfl.descriptors.quippy
import wfl.select.by_descriptor
import wfl.fit.gap.simple
from wfl.calculators import generic
from wfl.autoparallelize import AutoparaInfo
from wfl.autoparallelize import RemoteInfo
from wfl.generate import smiles
from wfl.utils.configs import atomization_energy
from wfl.select.simple import by_bool_func
from wfl.fit import error
from wfl.fit.gap.multistage import prep_params
from wfl.fit.gap.simple import run_gap_fit
from wfl.fit.gap.multistage import fit as gap_fit
import wfl.map
from pathlib import Path
import json
import argparse
import yaml
from expyre.resources import Resources
from pymatgen.io.ase import AseAtomsAdaptor
from vasppy.rdf import RadialDistributionFunction
import matplotlib.pyplot as plt

# https://matplotlib.org/stable/tutorials/introductory/customizing.html
plt.rcParams.update({"font.size": 15, 
                     "axes.linewidth": 2, # The width of the axes lines
                     "axes.labelsize": 15,
                     "axes.titlesize": 15,
                     "xtick.labelsize": 13,
                     "ytick.labelsize": 13,
                     "lines.linewidth": 2,
                     "lines.markersize": 5,
                     "legend.fontsize": 13,
                     "legend.markerscale": 2.0, # the relative size of legend markers vs. original
                     "legend.framealpha": 0.5,   # legend patch transparency
                    #  "scatter.markersize": 3
                     })
colors = [
    "#005555",  #0 fhi-aims green
    "#2470a0",  #1 muted blue
    "#ca3e47",  #2 muted red
    "#f29c2b",  #3 muted orange
    '#1f640a',  #4 muted green
    "#2ca02c",  #5 cooked asparagus green
    "#9467bd",  #6 muted purple
    "#8c564b",  #7 chestnut brown
    "#e377c2",  #8 raspberry yogurt pink
    "#7f7f7f",  #9 middle gray
    "#bcbd22",  #10 curry yellow-green
    "#17becf",  #11 blue-teal
]

markers = [
'o',#0       circle 
'v',#1       triangle_down marker
's',#2       square
'*',#3       star
'p',#4       point marker
'P',#5       pentagon marker
',',#6       pixel marker
'x',#7       x marker
]
linestyles = [
'-',#        solid line style
'--',#       dashed line style
'-.',#       dash-dot line style
':',#       dotted line stylecode 
(0, (3, 5, 1, 5, 1, 5)),#   dashdotdotted '- .. - .. - .. '
 (0, (5, 5)),#  dashed '-  -  -  -  -  -'
]



def get_formation_energy_per_atom(atoms:Atoms, ref_property:list, ref_formation:Atoms = None, ref_arg:int = None) -> list:

    """
    Get formation energy reference to reference structure

    Parameters:
    ----------
    atoms(Atoms): ase.Atoms object

    ref_property(list): list of label for atoms.info/arrays
        e.g. ['energy', 'forces']

    ref_arg(int): index of reference structure

    Returns:
    -------
    formation_energy_per_atom(list): formation energy per atom
    """

    energy_label = ref_property[0]


    if ref_formation is None:
        formation_energy_per_atom = [(mole.info[energy_label] - atoms[ref_arg].info[energy_label]) / mole.get_global_number_of_atoms() for mole in atoms]


    else:
        # default the energy_label for formation reference is energy
        ref_formation_energy_per_atom = ref_formation.info['energy'] / ref_formation.get_global_number_of_atoms()
        formation_energy_per_atom = [(mole.info[energy_label] / mole.get_global_number_of_atoms() - ref_formation_energy_per_atom) for mole in atoms]

    return formation_energy_per_atom


def gap_fit_simple(ref_dataset_file:str, ref_property:list, fitting_dict:dict):
    """
    To fit a gap model

    Parameters:
    ----------
    ref_dataset(Atoms): the filename of reference dataset for training
        e.g. 'train.xyz'

    ref_property(list): list of label for atoms.info/arrays
        e.g. ['energy', 'forces']

    Returns:
    -------
        GAP.xml model

    """

    if os.path.exists('fit'):
        os.chdir('fit')
    else:
        os.mkdir('fit')
        os.chdir('fit')

    # with open("./gap_fit_parameters.json", "r") as stream:
    #     fitting_dict = json.load(stream)

    remote_info = {
    "sys_name" : "null",
    "job_name" : "gap-fit",
    "resources" : {
        "max_time" : "24h",
        "num_cores" : 72,
        "partitions" : "general"},
    "check_interval": 5,
}

    run_gap_fit(fitting_configs=ConfigSet('../' + ref_dataset_file),
            fitting_dict=fitting_dict, 
            stdout_file='gap_fit.out',
            gap_fit_command="gap_fit",
            verbose=True,
            do_fit=True,
            remote_info=remote_info
            )
    os.chdir('../')

    return 0

def get_rdf_pair(in_config_atoms:str, species_i:str, species_j:str):
    """
    Get the rdf for a pair of species.
    """

    # Convert ASE atoms to pymatgen Structure using get_structure
    pymatgen_structures = [AseAtomsAdaptor.get_structure(ase_atoms) for ase_atoms in in_config_atoms]

    # Calculate RDF using vasppy
    rdf_pair = RadialDistributionFunction.from_species_strings(structures=pymatgen_structures, species_i=species_i, species_j=species_j)

    return rdf_pair

def get_correct_gap_params(in_config:str, uni_gap_params:dict, Zs:list) -> dict:
    """
    Correct unnecessary 2b descriptors in the universalSOAP parameters.

    Parameters
    ----------
    in_config: str
        The path to the input configuration file.

    uni_gap_params: dict
        The universalSOAP parameters.

    Zs: list
        The list of atomic numbers in the input configuration file.

    Returns
    -------
    neu_gap_params: dict
        The corrected universalSOAP parameters.
    """

    in_config_atoms = ase.io.read(in_config, ':')
    # rdf = aa.compute_rdfs_traj_avg(traj, rmax=4.0, nbins=50)

    # Zs_pair = list(set([tuple(sorted([Zs[i], Zs[j]])) for i in range(len(Zs)) for j in range(i, len(Zs))]))

    # Plot the rdf for each pair
    n = len(Zs)
    m = 0
    fig, ax = plt.subplots(n,n, figsize=(10,10), dpi=150, sharex=True, sharey=False)

    from ase.data import chemical_symbols
    for i in range(len(Zs)):
        for j in range(len(Zs)):
            if j < i:
                ax[i,j].axis('off')
                continue
            symbols_i, symbols_j = chemical_symbols[Zs[i]], chemical_symbols[Zs[j]]
            rdf_pair = get_rdf_pair(in_config_atoms, symbols_i, symbols_j)
            pair_name = f'{chemical_symbols[Zs[i]]}-{chemical_symbols[Zs[j]]}'
            pair_list = [Zs[i], Zs[j]]
            # rdf_pair = rdf[0][pair_name]
            
            # Get information from the universalSOAP parameters
            uni_pair = uni_gap_params['stages'][0]['descriptors'][m]['descriptor']['Z']
            print(pair_list, uni_pair)
            assert pair_list == uni_pair, "Warning: The atomic numbers in the input configuration file do not match the atomic numbers in the universalSOAP parameters."

            cutoff_pair = uni_gap_params['stages'][0]['descriptors'][m]['descriptor']['cutoff']

            
            # Collect the rdf data for distance less than the cutoff

            ax[i,j].plot(rdf_pair.r, rdf_pair.rdf, label=pair_name, color=colors[0])
            ax[i,j].vlines(cutoff_pair, 0, max(rdf_pair.rdf), color='k', linestyles='--')
            ax[i,j].set_title(f"{pair_name}")
            # ax[i,j].legend()


            # delete this descriptor from descriptors list
            rdf_pair_less_than_cutoff = [rdf_pair.rdf[i] for i in range(len(rdf_pair.r)) if rdf_pair.r[i] < cutoff_pair]

            if sum(rdf_pair_less_than_cutoff) <= 0.1:
                del uni_gap_params['stages'][0]['descriptors'][m]
                print(f"Remove the 2b descriptor for {pair_name}, since no environment within cutoff {cutoff_pair}.")
                continue
            m += 1

    fig.tight_layout()
    fig.savefig('rdf.png', dpi=150)

    return uni_gap_params

def error_callback(exception):
        print('Exception:', exception)

# def parallel_repulsive(dftb_band, dft_data, opt_elem, crep, kxc):
def parallel_valuation(val_dataset_file:str, num_parall, gap_model_file:str='fit/GAP.xml'):
    """
    Be parallelized !!!
    """

    import multiprocessing as mp
    import math
    # 1. divide input file into files
    # initialize information
    val_dataset_with_gap = val_dataset_file.replace('.xyz', '_gap.xyz')
    val_dataset = ase.io.read(val_dataset_file, ':')


    num_cores = int(mp.cpu_count())
    print(f"Parallel/Cpu: {num_parall}/{num_cores}")
    num_atoms = len(val_dataset)
    
    # seperate val_dataset into num_parall parts
    # val_dataset_list = [val_dataset[i::num_parall] for i in range(num_parall)]
    size_subs = math.ceil(num_atoms / num_parall)

    subdataset_names = [f"tmp.{val_dataset_file.split('.')[0]}_{i}.xyz" for i in range(num_parall)]
    subdataset_gap_names = [f"tmp.{val_dataset_file.split('.')[0]}_{i}_gap.xyz" for i in range(num_parall)]

    for n in range(num_parall):
        start_index = n * size_subs
        end_index = (n + 1) * size_subs
        subdataset = val_dataset[start_index: end_index]

        subdataset_name = subdataset_names[n]  
        ase.io.write(subdataset_name, subdataset)

    print("Seperate validation dataset into " + str(num_parall) + " parts for parallel calculation.")

    # with mp.Pool(processes=num_parall) as pool:
    #     input_parameters = [(subdataset_name, gap_model_file) for subdataset_name in subdataset_names]
    #     outputs = pool.imap(validation_calc, input_parameters)      

    #     for output in outputs:
    #         print(output)

    # # 2. open a pool
    pool = mp.Pool(processes=num_parall)
    for m in range(num_parall):
        # input_parameters = (subdataset_names[m], gap_model_file)
        pool.apply_async(validation_calc, args=(subdataset_names[m], gap_model_file), 
                                   error_callback=error_callback)

    pool.close()
    pool.join()

    print("Parallel calculation finished.")


    # # 3. collect results
    cat_command = '  '.join(subdataset_names)
    cat_command_gap = '  '.join(subdataset_gap_names)
    os.system(f"cat {cat_command_gap} > {val_dataset_with_gap} && rm -r {cat_command} {cat_command_gap}")  


    # print("CPU cores: " + str(num_cores))
    # # pool = mp.Pool(processes=len(elem_symbol))
    # pool = mp.Pool()

    # # Now distribute jobs to the processes
    # num_atoms = int(len(dft_data)/11)




def get_gap(in_file, gap_name, Zs, length_scales, params,
        ref_property_prefix='DFT_', run_dir='GAP'
    ):
    """
    Run the wfl.fit.gap_multistage fit function.

    Parameters
    ----------
    in_file:                str
        Path to file containing the input configs for the GAP fit
    gap_name:               str
        File name of written GAP
    Zs:                     list
        List of atomic numbers in the GAP fit.
    length_scales:          dict
        Length scale dictionary for each atomic species in the fit.
        Dictionary keys are the atomic numbers, values are dictionaries that
        must contain the keys "bond_len" and "min_bond_len"
    params:                 dict
        GAP fit parameters, see the parameter json files for more information
    ref_property_prefix:    str, default="DFT_"
        label prefixes for the in_config properties.
    run_dir:                str, default='GAP'
        Name of the directory in which the GAP files will be written
    Returns
    -------
    None, the selected configs are written in the out_file
    """
    in_config = ConfigSet(in_file)
    uni_gap_params = prep_params(Zs, length_scales, params)
    gap_params = get_correct_gap_params(in_file, uni_gap_params, Zs)
    print("Full gap params:", gap_params)
    gap_fit(in_config, gap_name, gap_params,
        run_dir=run_dir, ref_property_prefix=ref_property_prefix
    )
    return None

def prep_gnuparallel(val_dataset_file:str, num_parall, gap_model_file:str='fit/GAP.xml'):
    """
    Be parallelized !!!
    """

    # 1. divide input file into files
    # initialize information
    val_dataset_with_gap = val_dataset_file.replace('.xyz', '_gap.xyz')
    val_dataset = ase.io.read(val_dataset_file, ':')

    num_atoms = len(val_dataset)
    
    # seperate val_dataset into num_parall parts
    # val_dataset_list = [val_dataset[i::num_parall] for i in range(num_parall)]
    size_subs = math.ceil(num_atoms / num_parall)

    subdataset_names = [f"tmp.{val_dataset_file.split('.')[0]}_{i}.xyz" for i in range(num_parall)]
    subdataset_gap_names = [f"tmp.{val_dataset_file.split('.')[0]}_{i}_gap.xyz" for i in range(num_parall)]
    print(f"subdataset_names: {'  '.join(subdataset_names)}")
    print(f"subdataset_gap_names: {'  '.join(subdataset_gap_names)}")

    cmd_file = open("cmd.lst", 'w')
    for n in range(num_parall):
        start_index = n * size_subs
        end_index = (n + 1) * size_subs
        subdataset = val_dataset[start_index: end_index]

        subdataset_name = subdataset_names[n]  
        ase.io.write(subdataset_name, subdataset)
        cmd_file.write(f"python run.py -val {subdataset_name} >> output\n")
    
    cmd_file.close()

    print("Seperate validation dataset into " + str(num_parall) + " parts for parallel calculation.")

    os.system(f"bash gnuparallel.sh {num_parall}")

    os.system(f"cat {' '.join(subdataset_gap_names)} > {val_dataset_with_gap}")

    gap_dataset = ase.io.read(val_dataset_with_gap, ':')
    if len(gap_dataset) == len(val_dataset):
        os.system(f"rm -r {' '.join(subdataset_names)} {' '.join(subdataset_gap_names)}")
    else:
        print(f"WARNING: len(ref_dataset):{len(val_dataset)} != len(gap_dataset):{len(gap_dataset)}")

def validation_calc(val_dataset_file, gap_model_file):
    """
    Calculate the energy and forces for validation dataset/test dataset in order to evaluate the model

    Parameters:
    ----------
    val_dataset_file(str): the filename of validation dataset for training
        e.g. 'validation.xyz'

    ref_property(list): list of label for atoms.info/arrays
        e.g. ['energy', 'forces']

    gap_model_file(str): the filename of GAP model
        e.g. './fit/GAP.xml'
    

    Returns:
    -------
        file with energy and forces
    """
    # val_dataset_file, gap_model_file = input_parameters
    val_dataset_with_gap = val_dataset_file.replace('.xyz', '_gap.xyz')


    inputs = ConfigSet([val_dataset_file])
    outputs = OutputSpec([val_dataset_with_gap])

    gap_calc = (Potential, [], {"param_filename":gap_model_file})
    # print("validation_calc: ", val_dataset_file, gap_model_file)
    generic.calculate(
        inputs=inputs,
        outputs=outputs,
        calculator=gap_calc,
        properties=["energy", "forces"],
        output_prefix="gap_",
    #    autopara_info=gap_calc_autopara_info,
        )
    return  val_dataset_with_gap
    
def get_error(#ref_dataset:Atoms, gap_dataset:Atoms,
              gap_dataset_file:Atoms,
              property_prefixs:list,
#              ref_property_prefix:str, gap_property_prefix:str,
              weights:list=[1.0,0.1],
              calc_fromation_tag:bool=False) -> dict:
    """
    Calculate error for energy and forces based on reference and validation dataset(calculated with gap)

    Parameters:
    ----------
    ref_dataset(Atoms): ase.Atoms object for reference dataset
    
    gap_dataset(Atoms): ase.Atoms object for validation dataset(calculated with gap)

    ref_property_prefix(str): prefix for atoms.info/arrays energy and forces
        e.g. 'dft_'

    gap_property_prefix(str): prefix for atoms.info/arrays energy and forces
        e.g. 'gap_'
    
    formation_tag(bool): whether to calculate formation energy or not
        If one already store formation/relative energy in training file, then set it to False
        e.g. True or False
    
    Returns:
    -------
    error(dict): error for energy and forces
        e.g. {'energy': {'RMSE':0.01, 'MAE': 0.01, 'NUM': 100}, 
              'forces': {'RMSE':0.01, 'MAE': 0.01, 'NUM': 100}}}}    
    """
    ref_property_prefix, gap_property_prefix = property_prefixs
    ref_property = [ref_property_prefix + 'energy', ref_property_prefix + 'forces']
    gap_property = [gap_property_prefix + 'energy', gap_property_prefix + 'forces']
    # sigma_E, sigma_F = sigmas
    # dict_name = gap_dataset_file.split('.')[0].split('_')[0]
    weight_E, weight_F = weights

    gap_dataset = ase.io.read(gap_dataset_file, ':')

    if not calc_fromation_tag:
        ref_energy_per_atom = [mole.info[ref_property[0]] / mole.get_global_number_of_atoms() for mole in gap_dataset]
        gap_energy_per_atom = [mole.info[gap_property[0]] / mole.get_global_number_of_atoms() for mole in gap_dataset]

    else:
        ref_energy_per_atom = get_formation_energy_per_atom(gap_dataset, ref_property)
        gap_energy_per_atom = get_formation_energy_per_atom(gap_dataset, gap_property)
    
    ref_forces = [mole.get_array(ref_property[1]) for mole in gap_dataset]
    gap_forces = [mole.get_array(gap_property[1]) for mole in gap_dataset]

    NUM = len(gap_dataset)


    # RMSE & MAE for energy
    energy_rmse = np.sqrt(np.mean(np.square(np.array(ref_energy_per_atom) - np.array(gap_energy_per_atom))))
    energy_mae = np.mean(np.abs(np.array(ref_energy_per_atom) - np.array(gap_energy_per_atom)))

    # RMSE & MAE for forces
    forces_rmse = np.sqrt(np.mean(np.square(np.array(ref_forces) - np.array(gap_forces))))
    forces_mae = np.mean(np.abs(np.array(ref_forces) - np.array(gap_forces)))

    # Change units of energy and forces from kcal/mol to eV
    energy_rmse *= 0.0433641
    energy_mae *= 0.0433641
    forces_rmse *= 0.0433641
    forces_mae *= 0.0433641

    error = {'energy': {'RMSE':energy_rmse, 'MAE': energy_mae, 'NUM': NUM, 'unit': 'eV/atom'},
             'forces': {'RMSE':forces_rmse, 'MAE': forces_mae, 'NUM': NUM, 'unit': 'eV/Angstrom'},
             'LOSS':{'RMSE':energy_rmse * weight_E + forces_rmse * weight_F, 'NUM': NUM,
                     'MAE': energy_mae * weight_E + forces_mae * weight_F, 'NUM': NUM}}
    
    
    return error

def fit_and_error(#dataset_files:list=['train.xyz', 'validation.xyz'], 
                  gap_model_file:str='fit/GAP.xml', 
                  property_prefixs:list=['dft_', 'gap_'],
                  multistage_files:list=None, args:argparse=None) -> dict:
    """
    Fit a GAP model and calculate the error for energy and forces, designed for hyperparameter selection

    Parameters:
    ----------
    dataset_files(list): list of filenames for reference and validation dataset
        e.g. ['train.xyz', 'validation.xyz']
    
    gap_model_file(str): the filename of GAP model
        e.g. 'fit/GAP.xml'

    property_prefixs(list): list of label for atoms.info/arrays
        e.g. ['dft_', 'gap_']

    multistage_files(list): list of filenames for length_scales and multistage_params
        e.g. ['length_scales.yaml', 'multistage_params.json']

    Returns:
    -------
    error(dict): error for energy and forces
    
    """

    # Get reference and validation dataset, property prefixs for reference and GAP model
    # ref_dataset_file, val_dataset_file = dataset_files
    # ref_dataset_file, val_dataset_file = args.
    
    ref_property_prefix, gap_property_prefix = property_prefixs

    # Get "GAP.xml" and directory "fit"
    gap_name, GAP_dir = gap_model_file.split('/')[-1].split('.')[0], gap_model_file.split('/')[0]

    # Read length_scales and multistage_params
    length_scales_file, multistage_params_file = multistage_files
    with open(length_scales_file, 'r') as stream:
        length_scales = yaml.safe_load(stream)

    with open(multistage_params_file, 'r') as stream:
        multistage_params = json.load(stream)

    # sigma_E, sigma_F, _, _ = multistage_params['gap_params']['default_sigma']

    #=================GAP fit========================#
    if args.gap_fit:
        # check whether fit/GAP.xml exists and last line of fit/gap_out.txt == "libAtoms::Finalise: Bye-Bye!"
        ref_dataset_file = args.gap_fit
        if os.path.exists(gap_model_file) and Path(GAP_dir + '/gap_out.txt').read_text().split('\n')[-2] == "libAtoms::Finalise: Bye-Bye!":
            print("GAP model already exists, skip fitting.")


        else:
            # Fit GAP model with multistage way
            
            # Read Zs from training dataset
            ptmp_atoms = ase.io.read(ref_dataset_file)
            Zs = sorted(list(set(ptmp_atoms.get_atomic_numbers())))
            print(f"Zs={Zs}")

            get_gap(ref_dataset_file, gap_name, Zs, length_scales, 
                    multistage_params, ref_property_prefix=ref_property_prefix,
                    run_dir=GAP_dir)
            # gap_fit_simple(ref_dataset_file, ref_property, fitting_dict) # simple gap


    if args.gnuparallel:
        validation_file, num_parallel = args.gnuparallel[0], int(args.gnuparallel[1])
        prep_gnuparallel(validation_file, num_parallel, gap_model_file)

    #=================GAP validation=================#
    if args.validate_gap:

        
        for dataset_file in args.validate_gap:
            # val_dataset_file = args.validate_gap
            print(f"Validation for {dataset_file}")
            validation_calc(dataset_file, gap_model_file)

    #=================GAP error======================#
    if args.error_calculator:

        # ref_dataset = ase.io.read(ref_dataset_file, index=':')
        if os.path.exists('error.json') and os.path.getsize('error.json') > 0 and type(json.load(open("error.json", "r"))) == dict:
            # errors = open("error.json", "a")
            with open("error.json", "r") as f:
                errors = json.load(f)
        else:
            errors = {}

        for dataset_file in args.error_calculator:
            print(f"Error calculation for {dataset_file}")
            dict_name = dataset_file.split('.')[0].split('_')[0]
            # val_dataset = ase.io.read(val_dataset_file, ':')
            # gap_dataset = ase.io.read(val_dataset_file.replace('.xyz', '_gap.xyz'), index=':')
            weights = [1, 0.1] # weights for energy and forces

            error = get_error(dataset_file, property_prefixs, weights, calc_fromation_tag=False)
            errors[dict_name] = error

        # print(errors)
        
        with open('error.json', 'w') as f:
            json.dump(errors, f, indent=4)

        return errors


def molecule_dynamic_calc(initial_structure_file:str, gap_model_file:str, md_dict:dict=None):
    """
    Do MD simulation with GAP model
    """

    md_dataset_with_gap = initial_structure_file.replace('.xyz', '_gap.xyz')
    inputs = ConfigSet([initial_structure_file])
    outputs = OutputSpec([md_dataset_with_gap])

    gap_fit_calc = (Potential, [], {"param_filename":gap_model_file})

    ci = md.md(
        inputs=inputs, 
        outputs=outputs,
        calculator=gap_fit_calc,
        steps = md_dict['steps'], 
        dt = md_dict['dt'],           
        temperature = md_dict['temperature'],
        temperature_tau = md_dict['temperature_tau'],
        fraction = md_dict['fraction'],
        traj_step_interval = md_dict['traj_step_interval'],
        results_prefix = md_dict['results_prefix'],
        update_config_type = md_dict['update_config_type'],
        temporary_traj = md_dataset_with_gap,
        # autopara_info = md_dict['autopara_info']
        # autopara_info = AutoparaInfo(
        #     remote_info=remote_info, 
        #     num_inputs_per_python_subprocess=1)
        )



# The fitting parameters are stored in ./gap_fit_parameters.json
import sys
# sys.argv = ['run.py', '-gap', 'train.xyz', '-val', 'validation.xyz',  -err', 'validation_gap.xyz', '-md', 'ini.xyz']
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')

    parser.add_argument('-gap', '--gap_fit', type=str,
                        help='Fit GAP.   \
                              eg. python -gap train.xyz')

    parser.add_argument('-gnu', '--gnuparallel', nargs='+',
                        help='seperate validation dataset into num_parall parts')
    
    parser.add_argument('-val', '--validate_gap', nargs='+',
                        help='use GAP model to do calculation. \
                              eg. python -val validation.xyz fit/GAP.xml')
    
    parser.add_argument('-err', '--error_calculator', nargs='+', type=str,
                        help='calculate error for datasets. \
                              eg. python -err validation_gap.xyz, training_gap.xyz .')
    
    parser.add_argument('-md', '--md_run', nargs='+',
                        help='run MD simulation with GAP model. \
                              eg. python -md ini.xyz')

    parser.add_argument('-opt', '--geometry_optimization', type=str,
                        help='run geometry_optimization')


args = parser.parse_args()



#=================The necessary files that One need to preprepare===============#
#    1. Dataset                                                                 
#       - training.xyz             
#       - validation.xyz
#    2. length_scales.yaml & multistage_gap_parameters.json files
#===============================================================================#



#=====================Informations that One need to specify=====================#
gap_model_file = "fit/GAP.xml"
ref_property_prefix, gap_property_prefix = 'dft_', 'gap_'
property_prefixs = [ref_property_prefix, gap_property_prefix]

# For universalSOAP 
length_scales_file = "/u/mncui/software/universalSOAP/calculations/length_scales_VASP_auto_length_scales.yaml"
# For multistage GAP
multistage_params_file = "multistage_gap_parameters.json"
multistage_files = [length_scales_file, multistage_params_file]
#===============================================================================#



if args.gap_fit or args.gnuparallel or args.validate_gap or args.error_calculator:
# TRAINING, VALIDATION, AND ERROR CALCULATION
    errors = fit_and_error(gap_model_file, property_prefixs, multistage_files, args)


if args.md_run:
# MOLECULAR DYNAMICS
    initial_structure_file, steps = args.md_run
    md_dict = {'steps': int(steps),
               'dt': 0.5,
               'temperature': 300.0,
               'temperature_tau': None,
               'fraction': 0.001,
               'traj_step_interval': 1,
               'results_prefix': 'gapmd_',
               'update_config_type': False,
               #'autopara_info': None
               }
    molecule_dynamic_calc(initial_structure_file, gap_model_file, md_dict)
    pass

from ase.optimize import BFGS
if args.geometry_optimization:
# GEOMETRY OPTIMIZATION
    initial_structure_file = args.geometry_optimization
    gap_soap = Potential(param_filename='../fit/GAP.xml')

    print("geometry optimization calculating")
    ini_atoms = ase.io.read(initial_structure_file)
    ini_atoms.set_calculator(gap_soap)

    dyn = BFGS(ini_atoms, trajectory='opt.traj', restart='_opt.pckl')
    dyn.run(fmax=0.05)


