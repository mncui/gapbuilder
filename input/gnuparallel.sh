#!/bin/bash -l
#SBATCH --no-requeue                    
#SBATCH --nodes=1                         
#SBATCH --ntasks-per-node=72             
#SBATCH --job-name=gap-fit-sigma_radio_10_1e-02            
#SBATCH --partition=general              
#SBATCH --time=19:00:00                    
#SBATCH -o ./tjob.out.%j                 
#SBATCH -e ./tjob.err.%j                 
#SBATCH --mem=2048000               


# export WFL_GAP_FIT_OMP_NUM_THREADS=72
export OMP_NUM_THREADS=1
set -x
SLURM_NTASKS_PER_NODE="$1"
# run, Forest, run,...
parallel --delay 0.2 --joblog task.log --progress --resume -j $SLURM_NTASKS_PER_NODE < cmd.lst
echo "#--- Job ended at `date`"




# eval "$(conda shell.bash hook)"          
# conda activate gap_workflow              
# export PYTHONPATH=/u/mncui/software/anaconda3/envs/gap_workflow/bin/python:{PYTHONPATH} 

# module purge                                                      
# module load gcc/11 mkl/2022.2 gsl/2.4 impi/2021.4 fftw-mpi/3.3.9  
# module load parallel/201807
# source ${MKLROOT}/env/vars.sh intel64                           
# export QUIP_ARCH=linux_x86_64_gfortran_openmp                     
# export QUIP_ROOT=/u/mncui/software/QUIP                           
# export QUIPPY_INSTALL_OPTS=--user

# export LD_PRELOAD=/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_core.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_intel_thread.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/mkl/2022.2.1/lib/intel64/libmkl_intel_lp64.so:/raven/u/system/soft/SLE_15/packages/x86_64/intel_oneapi/2022.3/compiler/2022.2.1/linux/compiler/lib/intel64_lin/libiomp5.so

## source {MKLROOT}/bin/mklvars.sh intel64

# this one echos each command (only after module scripts!)
 
# environment variables
 
# the actual binary that is run
 
# custom pre-command stuff
#p_job=`echo "$PWD" |sed 's/^\/raven//'`
#randdir=`tr -dc A-Za-z0-9 </dev/urandom | head -c 13 ; echo ''`
#p_scratch="/ptmp/`echo $p_job |cut -d '/' -f 3-`/$randdir/"
#mkdir -p $p_scratch
 
#cp -r * $p_scratch
#cd $p_scratch
 
#export SLURM_NTASKS_PER_NODE=10 
 
 
# custom post-command stuff
#cp -r * $p_job
#cd $p_job
 
# remove the requested files
# nothing to clean
 
